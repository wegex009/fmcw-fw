#ifndef __DEFINES_H__
#define __DEFINES_H__

// any #defines that should be made visible to all libraries
// and project source code should go here.

#ifndef ARM_MATH_CM7
#define ARM_MATH_CM7
#endif

#ifndef STM32F722xx
#define STM32F722xx
#endif

#ifndef STM32F7xx
#define STM32F7xx
#endif

#ifndef STM32F7_DISCOVERY
#define STM32F7_DISCOVERY
#endif

// USB vendor id and product ID are defined in tm_stm32_usb_device_cdc.c
//#define USB_USE_HS
#define USB_USE_FS
#define USB_USE_DEVICE

void NMI_Handler(void);
void HardFault_Handler(void);
void BusFault_Handler(void);
void UsageFault_Handler(void);

#endif /* __DEFINES_H__ */
