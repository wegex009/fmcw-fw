#ifndef __FW_H__
#define __FW_H__

#include "defines.h"
#include "stm32f722xx.h"
#include "stm32f7xx_hal.h"
#include "stm32f7xx_hal_rcc_ex.h"

#include "tm_stm32_delay.h"
#include "tm_stm32_general.h"
#include "tm_stm32_usart.h"

#endif /* __FW_H__ */
