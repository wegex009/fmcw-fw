#ifndef __FW_MMIC_H__
#define __FW_MMIC_H__

#define N_ADC_SAMPLES 64

extern uint16_t adc1_buf[N_ADC_SAMPLES];
extern uint16_t adc2_buf[N_ADC_SAMPLES];

/* prepare the hw peripherals for the infineon MMIC.
  returns 0 on success, nonzero on failure. */
int mmic_periphs_init();

void start_dac_output(void);

void get_data(void);

#endif
