#include <usb_serial.h>
#include <defines.h>

// void setup_tim2_ignore_this_for_now()
// {
//   __HAL_RCC_TIM2_CLK_ENABLE();
//   TIM_HandleTypeDef event_timer = { .Instance = TIM2 };
//   event_timer.Init.Prescaler = 53;  // 53999 = 1ms per count. 53 = 1us per count
//   event_timer.Init.CounterMode = TIM_COUNTERMODE_UP;
//   event_timer.Init.Period = 999;  // timer counts from 0 to this value, inclusive
//   event_timer.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//   event_timer.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
//   HAL_TIM_Base_Init(&event_timer);
//   HAL_TIM_Base_Start(&event_timer);
// }

// PG6 allows "device connected" emulation...?

static int usb_fs_ready = 0;
static int usb_hs_ready = 0;

int usbserial_init()
{
#ifdef USB_USE_FS
  TM_USBD_CDC_Init(TM_USB_FS);    // TM_USB_FS is USB full-speed
#endif
#ifdef USB_USE_HS
  TM_USBD_CDC_Init(TM_USB_HS);    // TM_USB_HS is USB high-speed
#endif
#ifdef USB_USE_FS
  TM_USBD_Start(TM_USB_FS);
#endif
#ifdef USB_USE_HS
  TM_USBD_Start(TM_USB_HS);
#endif
  
  return 0;
}


// called by the periodic task when the USB ready status changes.
// usb_mode is either TM_USB_FS or TM_USB_HS, is_ready is boolean.
// if usbserial_periodic_task is being called from an interrupt, this
// MUST NOT try to write or read from the USB port.
static void usbserial_handle_ready_status_changed(
  TM_USB_t usb_mode,
  int is_ready)
{
  if (usb_mode == TM_USB_FS)
    usb_fs_ready = is_ready;
  else if (usb_mode == TM_USB_HS)
    usb_hs_ready = is_ready;
}


// called by the periodic task when the USB settings change (e.g. 
// baudrate changed). usb_mode is either TM_USB_FS or TM_USB_HS.
// you can virtually always assume the usb_mode is ready. settings
// will point to a static TM_USBD_CDC_Settings_t containing the settings
// the host has configured.
// if usbserial_periodic_task is being called from an interrupt, this
// MUST NOT try to write or read from the USB port.
static void usbserial_handle_settings_changed(
  TM_USB_t usb_mode,
  TM_USBD_CDC_Settings_t const *const settings)
{
  
}


// the periodic USB serial task
void usbserial_periodic_task()
{
  static int fsen = 0;
  static int hsen = 0;
  static TM_USBD_CDC_Settings_t fsset = {};
  static TM_USBD_CDC_Settings_t hsset = {};
  int e;
  
  TM_USBD_CDC_Process(TM_USB_Both);
  
  // check if USB FS and HS are ready, call handlers if the
  // ready state has changed
  e = (TM_USBD_IsDeviceReady(TM_USB_FS) == TM_USBD_Result_Ok);
  if (e != fsen) {
    usbserial_handle_ready_status_changed(TM_USB_FS, e);
    fsen = e;
  }
  
  e = (TM_USBD_IsDeviceReady(TM_USB_HS) == TM_USBD_Result_Ok);
  if (e != hsen) {
    usbserial_handle_ready_status_changed(TM_USB_HS, e);
    hsen = e;
  }
  
  // check settings for both modes (but only do the check if
  // that mode is enabled). call handler if the mode changed.
  if (fsen) {
    TM_USBD_CDC_GetSettings(TM_USB_FS, &fsset);
    if (fsset.Updated)
      usbserial_handle_settings_changed(TM_USB_FS, &fsset);
  }
  
  if (hsen) {
    TM_USBD_CDC_GetSettings(TM_USB_HS, &hsset);
    if (hsset.Updated)
      usbserial_handle_settings_changed(TM_USB_HS, &hsset);
  }
}


UsbStatus usbserial_get_status()
{
  if (usb_hs_ready)
    return US_HIGH_SPEED;
  else if (usb_fs_ready)
    return US_FULL_SPEED;
  else
    return US_NOT_READY;
}

int usbserial_write(uint8_t *buf, int buflen)
{
  int result = 0;
  if ((buflen > 0xFFFF) || (buflen < 0))
    result = -2; // buflen is too big, or negative
  else {
    switch (usbserial_get_status()) {
    case US_HIGH_SPEED:
      result = TM_USBD_CDC_PutArray(TM_USB_HS, buf, buflen);
      break;
    case US_FULL_SPEED:
      result = TM_USBD_CDC_PutArray(TM_USB_FS, buf, buflen);
      break;
    default:
      result = -1; // US_NOT_READY
    }
  }
  return result;
}

int usbserial_read(uint8_t *buf, int buflen)
{
  int result = 0;
  if ((buflen > 0xFFFF) || (buflen < 0))
    result = -2; // buflen is too big, or negative
  else {
    switch (usbserial_get_status()) {
    case US_HIGH_SPEED:
      result = TM_USBD_CDC_GetArray(TM_USB_HS, buf, buflen);
      break;
    case US_FULL_SPEED:
      result = TM_USBD_CDC_GetArray(TM_USB_FS, buf, buflen);
      break;
    default:
      result = -1; // US_NOT_READY
    }
  }
  return result;
}
