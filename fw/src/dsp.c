#include "dsp.h"

#define FFTSIZE 64
#define SAMPLERATE 400000
#define CONVERSION FFTSIZE
#define RAMPRATE 2000000000000 // Hz/s 2*10^12

/* Array with constants for CFFT module */
/* Requires ARM CONST STRUCTURES files */
const arm_cfft_instance_f32 CFFT_Instances2[] = {
	{16, twiddleCoef_16, armBitRevIndexTable16, ARMBITREVINDEXTABLE__16_TABLE_LENGTH},
	{32, twiddleCoef_32, armBitRevIndexTable32, ARMBITREVINDEXTABLE__32_TABLE_LENGTH},
	{64, twiddleCoef_64, armBitRevIndexTable64, ARMBITREVINDEXTABLE__64_TABLE_LENGTH},
	{128, twiddleCoef_128, armBitRevIndexTable128, ARMBITREVINDEXTABLE_128_TABLE_LENGTH},
	{256, twiddleCoef_256, armBitRevIndexTable256, ARMBITREVINDEXTABLE_256_TABLE_LENGTH},
	{512, twiddleCoef_512, armBitRevIndexTable512, ARMBITREVINDEXTABLE_512_TABLE_LENGTH},
	{1024, twiddleCoef_1024, armBitRevIndexTable1024, ARMBITREVINDEXTABLE1024_TABLE_LENGTH},
	{2048, twiddleCoef_2048, armBitRevIndexTable2048, ARMBITREVINDEXTABLE2048_TABLE_LENGTH},
	{4096, twiddleCoef_4096, armBitRevIndexTable4096, ARMBITREVINDEXTABLE4096_TABLE_LENGTH}
};


// set up the dsp library. We should think about whether or not we should implement setup closer to tm_stm32_fft.c
int initialize_DSP(IQDSP_object* dsp_obj, dspReturn* dsp_ret_obj)
{
   dsp_obj->fft_size = FFTSIZE;
   dsp_obj->adc_output_count = 0;
   dsp_obj->dsp_out_count = 4; // this is the number of dsp max values we want


   // setup the arm_cfft_instance_f32 status bits.
   dsp_obj->s = &CFFT_Instances2[2]; //currently set to 64 since we want 40 r/im samples

	 dsp_obj->dsp_return = dsp_ret_obj;
	 dsp_obj->dsp_return->count = dsp_obj->dsp_out_count;

   // output buffer. This buffer should be twice the size of our expected fourier
   // transform size. We are expecting 2 inputs. 1 real and 1 imaginary.
   dsp_obj->adc_outputIQ = (float32_t *) LIB_ALLOC_FUNC((dsp_obj->fft_size)*2*sizeof(float32_t));

   //Output buffer. This buffer should be the size of the expected fft.
   dsp_obj->fft_output = (float32_t *)LIB_ALLOC_FUNC((dsp_obj->fft_size) * sizeof(float32_t));

   // set the length of the dsp output arrays in structure
   dsp_obj->dsp_return->frequency = (float32_t *) LIB_ALLOC_FUNC( 4 * sizeof(float32_t));
   dsp_obj->dsp_return->magnitude = (float32_t *) LIB_ALLOC_FUNC((dsp_obj->dsp_out_count) * sizeof(float32_t));
   dsp_obj->dsp_return->calculatedDistance = (float32_t *) LIB_ALLOC_FUNC((dsp_obj->dsp_out_count) * sizeof(float32_t));


   if(dsp_obj->fft_output == NULL || dsp_obj->adc_outputIQ == NULL) // as of right now not verified to work
   {
     LIB_FREE_FUNC(dsp_obj->adc_outputIQ);
		 LIB_FREE_FUNC(dsp_obj->fft_output);

		 return 1;
   }

	 if(dsp_obj->dsp_return->frequency == NULL || dsp_obj->dsp_return->magnitude == NULL || dsp_obj->dsp_return->calculatedDistance == NULL) // as of right now not verified to work
	 {
		 LIB_FREE_FUNC(dsp_obj->dsp_return->frequency);
		 LIB_FREE_FUNC(dsp_obj->dsp_return->magnitude);
		 LIB_FREE_FUNC(dsp_obj->dsp_return->calculatedDistance);

		 return 1;
	 }

	 return 0;
}

// take FFT of adc output buffer and in fft_output display the fft magnitude
void computeFFT(IQDSP_object* dsp_obj)
{
  // Take the FFT of the addData
  // Take the magnitude of the data

  // we take the FFT of the ADC output here
  arm_cfft_f32(dsp_obj->s, dsp_obj->adc_outputIQ, 0, 1);

  // here we take the magnitude of the complex values of each bin
  arm_cmplx_mag_f32(dsp_obj->adc_outputIQ, dsp_obj->fft_output, dsp_obj->fft_size);

  // reset the count of our adc output for the next run of computing the FFT
  dsp_obj->adc_output_count = 0;
}

void transferBuffer_adcToDSP(IQDSP_object* dsp_obj, uint16_t* adcI, uint16_t* adcQ)
{
	for (int i = 0; i < FFTSIZE; i++) {
		dsp_obj->adc_outputIQ[2*i] = ((float32_t)adcI[i]) * ((3.3)/(4092));
		dsp_obj->adc_outputIQ[2*i + 1] = ((float32_t)adcQ[i]) * ((3.3)/(4092));
	}

}


// this function will place the magnitude and frequency of the search value into the mag and freq buffers, then shift the other values
void insertArrayValue(int bufferLocation, int bufferSize, float32_t searchMag, float32_t searchFreq, float32_t* mag, float32_t* freq)
{
	int i = bufferLocation;

	// first, we save off the values about to be replaced by the search values
	float32_t swapMag = mag[bufferLocation];
	float32_t swapFreq = freq[bufferLocation];

	float32_t tempMag = 0;
	float32_t tempFreq = 0;

	// next, we save the new mag and freq values to the buffer bufferLocation
	mag[bufferLocation] = searchMag;
	freq[bufferLocation] = searchFreq;

	i++; // increment the counter

	// now, we shift the values down
	for(i; i < bufferSize; i++)
	{
		// save current location values to temp variables
		tempMag = mag[i];
		tempFreq = freq[i];

		// set array location to swap values
		mag[i] = swapMag;
		freq[i] = swapFreq;

		// set swap values to temp values so the loop can repeat

		swapMag = tempMag;
		swapFreq = tempFreq;

	}


}

// this function will act the same as arm_math_f32.c
void parseFFTArray(IQDSP_object* dsp_obj)
{

  uint16_t count = 0u; // start our count at zero
  uint16_t maxCount = dsp_obj->fft_size/2; // set the dsp max count to the first half of the fft

  float32_t* dspOrigin = dsp_obj->fft_output;
  float32_t* curDspLoc = dsp_obj->fft_output; // this is our pointer to the current dsp dsp_object

	// set desp magnitude values to -1 for search loop
	dsp_obj->dsp_return->magnitude[0] = -1;
	dsp_obj->dsp_return->magnitude[1] = -1;
	dsp_obj->dsp_return->magnitude[2] = -1;
	dsp_obj->dsp_return->magnitude[3] = -1;


  float32_t searchMag = -1;
	float32_t searchLoc = 0;


  // for each count cycle we check until the halfway point of the buffer.
  for (count = 0; count < maxCount; count++)
  {
    searchLoc = count;
    searchMag = *(dspOrigin+count); // this may contain incorrect syntax

		if(searchMag > dsp_obj->dsp_return->magnitude[0])
		{
			insertArrayValue(0, 4, searchMag, searchLoc, dsp_obj->dsp_return->magnitude, dsp_obj->dsp_return->frequency);
		}
		else if(searchMag > dsp_obj->dsp_return->magnitude[1])
		{
			insertArrayValue(1, 4, searchMag, searchLoc,  dsp_obj->dsp_return->magnitude, dsp_obj->dsp_return->frequency);
		}
		else if(searchMag > dsp_obj->dsp_return->magnitude[2])
		{
			insertArrayValue(2, 4, searchMag, searchLoc, dsp_obj->dsp_return->magnitude, dsp_obj->dsp_return->frequency);
		}
		else if(searchMag > dsp_obj->dsp_return->magnitude[3])
		{
			insertArrayValue(3, 4, searchMag, searchLoc, dsp_obj->dsp_return->magnitude, dsp_obj->dsp_return->frequency);
		}
		else
		{
			// do nothing
		}


  }

    // now, process the data once we leave the file.

    dsp_obj->dsp_return->frequency[0] = dsp_obj->dsp_return->frequency[0] * (SAMPLERATE/((float)CONVERSION));
    dsp_obj->dsp_return->calculatedDistance[0] = dsp_obj->dsp_return->frequency[0] * (float32_t)300000000 * 0.5 * RAMPRATE ;

    dsp_obj->dsp_return->frequency[1] = dsp_obj->dsp_return->frequency[1] * (SAMPLERATE/((float)CONVERSION));
    dsp_obj->dsp_return->calculatedDistance[1] = dsp_obj->dsp_return->frequency[1] * (float32_t)300000000 * 0.5 * RAMPRATE ;

    dsp_obj->dsp_return->frequency[2] = dsp_obj->dsp_return->frequency[2] * (SAMPLERATE/((float)CONVERSION));
    dsp_obj->dsp_return->calculatedDistance[2] = dsp_obj->dsp_return->frequency[2] * (float32_t)300000000 * 0.5 * RAMPRATE ;

    dsp_obj->dsp_return->frequency[3] = dsp_obj->dsp_return->frequency[3] * (SAMPLERATE/((float)CONVERSION));
    dsp_obj->dsp_return->calculatedDistance[3] = dsp_obj->dsp_return->frequency[3] * (float32_t)300000000 * 0.5 * RAMPRATE ;

}

// code below can test STM32 FFT sample code
/*
// Init FFT, FFT_SIZE define is used for FFT_SIZE, samples count is FFT_SIZE * 2, don't use malloc for memory allocation
TM_FFT_Init_F32(&FFT, FFT_SIZE, 0);

// We didn't used malloc for allocation, so we have to set pointers ourself
// Input buffer must be 2 * FFT_SIZE in length because of real and imaginary part
// Output buffer must be FFT_SIZE in length
TM_FFT_SetBuffers_F32(&FFT, Input, Output);

i = 0;
// Let's fake sinus signal with 5, 15 and 30Hz frequencies
do {
	// Calculate sinus value
	sin_val = 0;
	sin_val += (float)5 * (float)sin((float)2 * (float)3.14159265359 * (float)freq * (float)time) + 1;

	time+= sampleShift;
	//dsp_obj.adc_outputIQ[2*i] = sin_val;

	i++;

} while (!TM_FFT_AddToBuffer(&FFT, sin_val));
// FFT sample practice
TM_FFT_Process_F32(&FFT);

*/
