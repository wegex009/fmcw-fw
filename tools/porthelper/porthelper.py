# DO pip install pyserial BEFORE RUNNING THIS
# also set the COM port to whatever makes sense
import serial

with serial.Serial('COM6', 9600,
       parity=serial.PARITY_NONE,
       stopbits=serial.STOPBITS_ONE,
       bytesize=serial.EIGHTBITS) as port:
  
  # byte 0: packet kind (0 = echo)
  # byte 1: packet uid (not currently used, just set it to 0)
  # byte 2: packet protocol version (set it to 0)
  # byte 3: packet content_length (the number of bytes folloring byte 3, in this case 5)
  #   the rest of the bytes are the packet "contents." An echo packet's body is always
  #   the 5-byte null-terminated string "echo"
  port.write(b'\x00\x00\x00\x05echo\x00') # send an echo packet
  
  print("sent packet, awaiting response...")
  # a response to an echo packet is 9 bytes long
  response = port.read(9)
  
  print("RESPONSE PACKET:")
  print("kind =", response[0])
  print("uid =", response[1])
  print("protocol =", response[2])
  print("content_length =", response[3])
  print("content =", response[4:])
