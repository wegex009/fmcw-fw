# Introduction

## Some Nomenclature

Some terminology, as used in this README.

**Device**: The FMCW Radar board. Connects to a host over USB.

**Firmware**: The code that runs on the STM32 microcontroller.

**FMCW**: Stands for Frequency-Modulated Continuous-Wave. A special kind
of radar that is capable of detecting distance of an object, not just its
speed. Used in automotive applications, for collision detection. Potential
applications include robotics, drones, industrial automation...

**HAL**: Stands for Hardware Abstraction Layer. A low-level library that
provides access to a microcontroller's peripheral registers, interrupts,
and the like.

**Host**: Whatever you connect the device to. If you connect the device to
your Android phone, the phone is the host. Each type of host we want to
support will need an SDK.

**Packet Protocol**: The "language" spoken between the host and device.
Code for encoding and decoding packets has been defined in the common
directory.

**Request Packet**: A packet sent from the host to the device.

**Response Packet**: A packet sent from the device to the host.

**SDK**: Stands for Software Development Kit. Really just a more pretentious
way of saying "a big library." e.g: we provide an SDK that will allow users to
create Android apps that interface with the device.

## Project structure

- `fw`: The firmware source code. This is the stuff that runs on the STM32.
  - `fw/inc`: The firmware header files.
  - `fw/src`: The firmware source code files.
- `lib`: Libraries used by the firmware.
  - `lib/examples`: examples of using the stm32 library, described below.
  - `lib/stm32`: the stm32 library. This is the high-level library, providing
    heavily abstracted functionality.
  - `lib/hal/CMSIS`: the CMSIS library. This is a low-level library, providing
    basic functionality shared across all Cortex-M microcontrollers.
  - `lib/hal/STM32F7xx_HAL_Driver`: the STM32F7 HAL library. Provides basic
    functionality specific to the STM32F722 microcontroller.
- `sdk`: SDKs for using the device. Currently, we have only made one SDK.
  - `sdk/android`: The FMCW Android SDK. Lets you use the device with Android.
- `common`: Code that is used both by the SDKs, and by the firmware. The
  most important part of the common code is the packet protocol, which encodes
  and decodes the messages sent between the device and host.
- `tools`: Originally intended to be a place where we put various tools that
  make it easier to work with the project. Not really much here.
  - `tools/porthelper`: A python script that opens a connection with the device,
    sends an echo packet, and waits for a reply from the device.
- `build`: The various files spat out by the firmware build process go in
  this directory.
- `defines.h`: A header file used by the libraries and firmware. This should
  probably be put under `common/` eventually- doing that will require some
  tweaks to the library makefiles.

# Getting Ready

The sections below provide instructions for getting your computer set up to
work on various parts of the project.

## Installing Software for Firmware Development (Windows)

**Getting a Unix-Friendly version of Make**

1. Install [MSYS2-x86_64](https://www.msys2.org/). The installer's defaults
   are fine.
2. Once MSYS2 finishes installing, open an MSYS2 terminal. You can open MSYS2 by
   going to the start menu, and just typing in "MSYS2" then hitting enter.
3. In MSYS2, run this command: `pacman -Syuu`, and enter `Y` for Yes when it asks.
4. MSYS2 will now download and install its base packages. When it is done, it will
   say something like this: `warning: terminate MSYS2 without returning to shell and check for updates again`.
   When it says this, close MSYS2. When you try to close it, it might give you a
   warning about a running process. Ignore the warning.
5. Reopen MSYS2. Run `pacman -Syuu` again. It will probably take longer this time.
6. Now, run this: `pacman -Syuu mingw32/mingw-w64-i686-make`
7. Now, run this: `pacman -Syuu msys/make`
8. Add this to your Path environment variable: `C:\msys64\usr\bin`
9. You should now be able to open a normal Windows command prompt, and run `make`.

**Installing GCC for ARM Microcontrollers**

1. Install the [GNU ARM Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads). The right download link will have a filename
  that looks like this: `gcc-arm-none-eabi-<VERSION>-major-win32.exe`
2. If the installer asks if you want to add stuff to your PATH, say yes.
   If it doesn't ask, add this to your PATH environment variable:
   `C:\Program Files (x86)\GNU Tools Arm Embedded\<there's only one directory here, figure it out>\bin`
3. You should now be able to open a normal Windows command prompt, and run
   `arm-none-eabi-gcc --version`.

**Installing Ozone, The Firmware Debugger**

1. Install [Segger Ozone](https://www.segger.com/products/development-tools/ozone-j-link-debugger/).
   You'll have to scroll down a bit to get to the download link. It's under
   "Download & Installation"
2. It might ask you about licenses and stuff. Ignore it.

**What IDE do I need to install?**

It doesn't matter. The firmware isn't set up to use any particular IDE or text
editor, so you can use any editor you like. You'll do all your compilation in
the command prompt, and all your firmware uploading/debugging inside of
Ozone. If you have a text editor you like, my opinion already means nothing to
you. However, if you aren't sure what editor you want to use, popular options
for Windows include [VS Code](https://code.visualstudio.com/) and
[Atom](https://atom.io/).

## Installing Software for Firmware Development (Ubuntu, Debian)

**Getting a Unix-Friendly version of Make**

1. `sudo apt install build-essential make`

**Installing GCC for ARM Microcontrollers**

1. `sudo apt install gcc-arm-none-eabi`

**Installing Ozone, The Firmware Debugger**

1. Install [Segger Ozone](https://www.segger.com/products/development-tools/ozone-j-link-debugger/).
   You'll have to scroll down a bit to get to the download link. It's under
   "Download & Installation." Choose the option "Ozone - The J-Link Debugger
   for Linux, DEB installer, 64-bit" and click "Download."

2. `cd ~/Downloads; sudo apt install ./Ozone_Linux_<VERSION>_x86_64.deb`

**What IDE do I need to install?**

It doesn't matter. The firmware isn't set up to use any particular IDE or text
editor, so you can use any editor you like. You'll do all your compilation in
the terminal, and all your firmware uploading/debugging inside of Ozone. If you
have a text editor you like, my opinion already means nothing to you. However,
if you aren't sure what editor you want to use, popular options
for Debian and Ubuntu include [VS Code](https://code.visualstudio.com/) and
[GNU Emacs](https://www.gnu.org/software/emacs/).

## Installing Software for FMCW Android SDK Development

1. Follow Google's guide for installing [Android Studio.](https://developer.android.com/studio/install)
2. Open the SDK project by opening Android Studio, and clicking "Open an existing
   Android Studio project." Navigate to wherever you put the git repository, and
   go to `sdk/android`.
3. Android Studio might take a while to load some stuff. Gradle's chugging away,
   don't worry about it.
4. You may be prompted to install some version of the Android SDK, Android NDK,
   and/or CMake. Say yes to any of these.
5. Go to `Tools -> SDK Manager.` Make sure that "Android 9.+ (Q)" is selected in
   the list, then click "Apply" If the box is already checked, you don't need to
   do anything in this step.
6. Still in SDK Manager, click on "SDK Tools" (above the list of SDKs). Check
   the boxes to install "NDK (Side by Side)" and "CMake." Click "Apply."

# Compiling and Debugging the Firmware

Once you have followed the instructions above for setting up your system to
work on the firmware, open a terminal in the root of the git repository.

Inside of that terminal, run `make libraries`. This will compile the
library files (the stuff under the lib directory).

Then, run `make`. This will compile all of the firmware code (the stuff in 
the fw directory). It will also link all the object files together, producing
the binary that can be uploaded to the microcontroller. There will now be a
file in the build directory, called firmware.elf. This is the firmware binary.

To upload the new firmware to the STM32, plug the debugger side of the board
into your computer (the USB port on the side of the board that looks like you
can break it off). Then, open Ozone.

Ozone will automatically
open the New Project Wizard. In the wizard, choose the Device: STM32F722ZE.
Don't worry about the Peripherals box. Hit next.

The target interface is SWD,
Interface Speed is 1MHz, Host Interface is USB. Leave the Serial Number box
empty. Hit next.

Choose the .elf file that was produced when you compiled the firmware. This
file will be in the build directory: `build/firmware.elf`. Hit next.

Ozone will might show you some warnings about licenses, just ignore them.
Ozone will burn the firmware onto the board. There will then be a green
button in the top left corner. It will reset the board and start running
the firmware.

Ozone allows you to set breakpoints, read the stack, look at registers,
and get the values of variables in real-time. It also does a lot of other
stuff. Start watching [this tutorial](https://youtu.be/H2Yf-QeFsPw?t=100)
at 1:40 to get an idea for how to use Ozone.

# Compiling the FMCW Android SDK and Installing the Demo App on an Android Phone

## Opening the Android Studio Project

Open Android Studio. If you have opened the project before, it will open by
itself. If the project didn't open by itself, here's how to open it:

- **Opening the project from the Android Studio launcher:** Click on
  "Open an existing Android Studio project." Navigate the file tree to wherever
  you put your copy of the git repo. Select the `android` directory, under `sdk`.
  Click OK.

- **Opening the project when a different Android Studio project is already
  open:** Go to File -> Open. Navigate the file tree to wherever
  you put your copy of the git repo. Select the `android` directory, under `sdk`.
  Click OK.

## The Project Structure

There are two modules in the Android Studio project: the FMCW SDK itself, and
an example application called testapp.

Two output files are very important for the Android SDK. When you are looking
to get the distributable files, you must select the "release" build variant in
Android Studio.

- `sdk/android/fmcw/build/outputs/aar/fmcw-release.aar`: This is the SDK itself.
  this is the package you would distribute to users who want to make their
  own apps.
- `sdk/android/testapp/build/outputs/apk/release/testapp-release-unsigned.apk`
  This is the package containing the demo app. An APK can be installed onto
  an Android device. The APK is currently unsigned, so it can't be officially
  distributed on the Google Play Store. If you want to do that, you'll have
  to edit the Android Studio project to produce a signed APK.

# Future Project Tasks

## Support New Hosts

Currently, the only supported host is Android. However, since the device
communicates over plain old USB serial, pretty much anything with a USB port
could be supported.

Making a Python SDK would probably be a good idea. This would allow Windows,
MacOS, and Linux to all be supported hosts. In `tools/porthelper/porthelper.py`
you can see a bare-bones example of Python code sending an echo packet to the
device, then receiving an echo packet back. A proper implementation (making a
folder in `sdk` to hold the Python SDK, and making Python code call the
packet protocol functions in `common`), has been left as an exercise for future
developers.

You may also want to make an SDK for iOS.

You won't need to touch the firmware to do this. The Android FMCW SDK, and its
example app, should give you a good idea for how to create a powerful, 
easy-to-use SDK.

## Make A Cool App

Here's a pretty neat idea: if you take the data retrieved from the radar, and
combine it with a camera feed from an Android phone, you may be able to detect
objects in the camera feed, and label them with their distance from the device.

You will not need to touch the firmware to make this work (probably). However,
you'll need to add a new Android app to the Android Studio project (hint:
`file -> new module`), and do some pretty crazy camera stuff.

## Update Firmware over USB

Currently, there is no way to perform a firmware update over USB. Sure, you can
burn a new ELF file onto the device using the hardware debugger, but you don't
want customers to have to do that.

Firmware updates are a very important feature if the project is to be a
commercially-viable product. However, it is very time-consuming to implement,
and so it has been left as a (pretty challenging) exercise for future developers.

This is the hardest of the future project tasks listed here. Every aspect of
the project will need to be touched in order to add USB firmware updates. You
will need to tweak the inner workings of the firmware's compilation process,
modify linker scripts, and modify the nasty part of the Android FMCW SDK (the
nasty part being the `PacketProtocol` class). You will need a basic
understanding of how to use the Java Native Interface (JNI). Adding a
bootloader to the firmware will make it much more difficult to do debugging in
Ozone, as well.

In order to support firmware updates, you will need to do the following:

**1. Changes to Packet Protocol**

You will need a new kind of request packet for sending the new firmware binary
to the device. Remember that a packet can only have 256 bytes beyond its 4-byte
header, and that the device will only be able to receive a packet that's as
long as the USB RX and TX buffers you define.

Hint: your firmware is going to be bigger than 256 bytes, so you will need to
send multiple packets.

Hint: if you want to make your life much easier, you could get clever with the
existing write packet to write out the firmware without big changes to the
packet protocol. Remember that you can only write 16 bytes at a time using
write packets.

Hint: A .bin file is just a sequence of bytes, in the exact order they should
appear in the device's flash.

Hint: [This guide](https://interrupt.memfault.com/blog/how-to-write-a-bootloader-from-scratch#a-minimal-bootloader) 
uses `arm-none-eabi-objcopy` to produce a .bin file containing the firmware.

**2. Changes to Firmware**

Make a bootloader. [This guide](https://interrupt.memfault.com/blog/how-to-write-a-bootloader-from-scratch#a-minimal-bootloader)
should get you going in the right direction. Some tips:
- The guide linked above mentions the use of OpenOCD. OpenOCD is an
  alternative to Ozone, but the instructions in the guide are still relevant
  to this project.
- Linking (and use of the linker script) happens in `Makefile.2`, in the root
  of the repository.
- The linker script is located at `build/linker_script.ld`.
- The Reset_Handler function used by the firmware is located at
  `lib/hal/CMSIS/Device/ST/STM32F7xx/Source/Templates/gcc/startup_stm32f745xx.s`.
  Yes, the filename has a different STM32 microcontroller in its name. Don't
  worry about it.
- Remove the calls to `TM_RCC_InitSystem()`, `HAL_Init()`, `TM_DELAY_Init()`,
  and `usbserial_init()` from the firmware.
- Figure out how to make the firmware jump back into the bootloader (Hint:
  `NVIC_SystemReset()`)
- The bootloader will need to do this:
  - Figure out if the bootloader is running because the device just started up,
    or if it's running because the firmware called it.
    (Hint: `int software_reset = __HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST) != 0;`.
    The guide's way of doing it also works.)
  - If the bootloader is running because the device just powered up, or a hard
    reset happened:
      - Call `TM_RCC_InitSystem()`, `HAL_Init()`, `TM_DELAY_Init()`, and
        `usbserial_init()`, just like the firmware did.
      - Jump into the firmware, just like how the guide does it.
  - If the bootloader is running because the firmware caused it to run:
      - Wait for a packet to be sent over USB. Once packets start coming in,
        burn the new firmware into flash.
      - Jump into the firmware, once it has been written.

**3. Changes to SDK**

Add a new function to the `FmcwDevice` interface in the Android SDK.
Something like this might be good:

```java
  interface FirmwareUpdateCompletion {
    void exec(boolean didSucceed);
  }
  
  void firmwareUpdate(File binFile, FirmwareUpdateCompletion completion);
```

Obviously, `FmcwSimulatedDevice` should do nothing when this is called.
`FmcwUsbSerialDevice` should send out the packets needed to do the firmware
update. If you decided to be fancy, and create a new kind of packet for
writing firmware, you'll need to modify `PacketProtocol` and `bridge.c` to
make this work.

If you decide to create an SDK for another platform (like the Python or iOS
ones mentioned above), you'll need to make sure those SDKs can also do
firmware updates.
