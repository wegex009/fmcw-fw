#ifndef __FW_DEVPARMS_H__
#define __FW_DEVPARMS_H__

#include <stddef.h>
#include <stdint.h>

// There are 2 kinds of device parameters currently implemented:
//   - int32: 32-bit signed integer
//   - bool: boolean
// 2 more kinds are planned
//   - bytes: array of 16 bytes
//   - string: null-terminated string up to 15 characters (16 bytes) long

// there are persistent params and hook params.
// persistent params are variables that are burned into flash. they can
// be used for any kind of device setting that you want to save between
// power cycles. they are burned to flash each time param_commit() is called
// hook params can be anything you want. "hooks" (functions) for getting and
// setting them can be added to the hook table in device_params.c. There can
// be up to 65,280 hook params.


typedef enum ParamKind ParamKind;
typedef enum Parameter Parameter;

enum ParamKind {
  PKIND_INT32 = 1,
  PKIND_BOOL = 2,
  PKIND_BYTES = 3,
  // put more param kinds here if more are added
  
  PKIND_ERR = 0xFF // used for errors
};


enum Parameter {
  PARAM_PERSISTENT_EXAMPLE_INT32 = 0,  // a persistent parameter of the int32 type, named EXAMPLE
  
  // persistent params go above this line. do NOT explicitly set values for them (except the first one)
  PARAM_N_PERSISTENT,
  PARAM_BYTES_SERIAL_NUM = 0x100,
  PARAM_SINGLE_SCAN = 0x101,
  
  // hook params go above this line. do NOT explicitly set values for them (except the first one)
  PARAM_END_HOOK
};

#if defined(STM32F7xx)

// initialize the device params component. returns 0 on success.
int param_init();


// read a device parameter of an int32 type. if successful, the value
// of this parameter will be written into value. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not int32 type.
// returns 3 if the hook parameter arg is not gettable.
int param_get_int32(int parameter, int32_t *value);
// write a device parameter of an int32 type. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not int32 type.
// returns 3 if the hook parameter arg is not settable.
int param_set_int32(int parameter, int32_t value);


// read a device parameter of boolean type. if successful, the value
// of this parameter will be written into value. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not bool type.
// returns 3 if the hook parameter arg is not gettable.
int param_get_bool(int parameter, int *value);
// write a device parameter of boolean type. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not bool type.
// returns 3 if the hook parameter arg is not settable.
int param_set_bool(int parameter, int value);


// read n bytes of device parameter of bytes type. if successful, the value
// of this parameter will be written into value. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not bool type.
// returns 3 if the hook parameter arg is not gettable.
int param_get_bytes(int parameter, uint8_t *value, int *ngotten);
// write n bytes of device parameter of bytes type. return 0 on success.
// returns 1 if the parameter arg is invalid.
// returns 2 if the hook parameter arg is not bool type.
// returns 3 if the hook parameter arg is not settable.
int param_set_bytes(int parameter, uint8_t *value, int n);


// saves changes to device params to flash. this takes a LONG time.
void param_commit();

#endif // defined(STM32F7xx)

#endif
