#include <packet_protocol.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include "packet_protocol.h"

#define PDQ_BADKIND -1
#define PDQ_BADLEN -2
#define PDQ_RWBADPKIND -3
#define PDQ_OLDPROTOCOL -4

int packet_decode_request(uint8_t const *in, int inlen, RequestPacket *request)
{
  if ((!request) || (!in))
    return -1; // null ptr
  
  int result = 0;
  
  if (inlen >= 4) {
    uint8_t const pkind = in[0];
    request->kind = pkind;
    request->uid = in[1];
    request->revision = in[2];
    request->contents_len = in[3];
    
    if (inlen >= (4 + request->contents_len)) { // have the full packet
      if (pkind == PK_ECHO) {
        if (request->contents_len == 5) {
          memcpy(request->contents.echo.message, &in[4], request->contents_len);
          result = 9; // set result to number of bytes decoded
        } else
          result = PDQ_BADLEN; // invalid contents_len for an echo packet
      } else if (pkind == PK_WRITE_PARAMETER) {
        if (request->contents_len < 3)
          result = PDQ_BADLEN; // invalid content_len for any kind of param write
        else {
          uint8_t const parmkind = in[6];
          request->contents.write_parameter.param_kind = parmkind;
          request->contents.write_parameter.parameter = (in[4] << 8) | in[5];
          if ((parmkind == PKIND_INT32) && (request->contents_len == 7)) {
            uint32_t temp = ((uint32_t)in[7]) << 24;
            temp |= ((uint32_t)in[8]) << 16;
            temp |= ((uint32_t)in[9]) << 8;
            temp |= ((uint32_t)in[10]);
            // type-pun temp into a signed value
            request->contents.write_parameter.int_val = *((int32_t*)&temp);
            result = 11; // write int32 is an 11-byte packet
          } else if ((parmkind == PKIND_BOOL) && (request->contents_len == 4)) {
            request->contents.write_parameter.bool_val = in[7];
            result = 8; // write bool is an 8-byte packet
          } else if ((parmkind == PKIND_ERR) && (request->contents_len == 3)) {
            result = 7; // write err is a 7-byte packet
          } else if ((parmkind == PKIND_BYTES) && (request->contents_len <= 19)) {
            result = 4 + request->contents_len;
            memcpy(request->contents.write_parameter.bytes_val, in + 7, request->contents_len - 3);
          } else if ((parmkind == PKIND_BYTES) || (parmkind == PKIND_INT32) || (parmkind == PKIND_BOOL) || (parmkind == PKIND_ERR)) {
            // the param kind is valid, so the contents_len must be wrong
            result = PDQ_BADLEN;
          } else {
            result = PDQ_RWBADPKIND; // the param_kind is invalid
          }
        }
      } else if (pkind == PK_READ_PARAMETER) {
        if (request->contents_len == 3) {
          request->contents.read_parameter.parameter = (in[4] << 8) | in[5];
          uint8_t const parmkind = in[6];
          request->contents.read_parameter.param_kind = parmkind;
          if ((parmkind != PKIND_INT32) 
              && (parmkind != PKIND_BOOL) 
              && (parmkind != PKIND_ERR)
              && (parmkind != PKIND_BYTES))
            result = PDQ_RWBADPKIND; // invalid param_kind
          else
            result = 7;  // read param is a 7-byte packet
        } else {
          result = PDQ_BADLEN;  // invalid content_len for a param read
        }
      } else
        result = PDQ_BADKIND; // invalid request kind
    }
  }
  
  return result;
}

int packet_encode_response(uint8_t *buf, ResponsePacket const *const response)
{
  if ((!response) || (!buf))
    return -1; // null ptr
    
  int error_code = 0;
  
  // calculate contents length
  uint8_t contents_len = 0;
  if (response->kind == PK_ECHO) {
    contents_len = 5;
  } else if (response->kind == PK_READ_PARAMETER) {
    switch (response->contents.read_parameter.param_kind) {
    case PKIND_INT32: contents_len = 7; break; // add size of int32
    case PKIND_BOOL: contents_len = 4; break;  // add size of bool
    case PKIND_BYTES: contents_len = response->contents_len; break; // contents_len is dynamic, must be set by user outside function
    case PKIND_ERR: contents_len = 3; break;   // err packet still has 3 content bytes
    default: error_code = -2; break;  // incorrect value for param kind
    }
  } else if (response->kind == PK_WRITE_PARAMETER) {
    contents_len = 3;
  } else if (response->kind == PK_PRINT) {
    contents_len = strlen(response->contents.print.str) + 1;
    // cap contents_len to the max possible content length.
    if (contents_len > PACKET_RESPONSE_MAX_LEN - 4)
      contents_len = PACKET_RESPONSE_MAX_LEN - 4;
  } else
    return 0; // invalid packet kind
  
  int const alloc_len = contents_len + 4; // 4 bytes for the 4 RequestPacket fields
//   uint8_t *buf = malloc(sizeof(uint8_t) * alloc_len);
//   *out = buf;
  
  buf[0] = response->kind;
  buf[1] = response->uid;
  buf[2] = PACKET_PROTOCOL_REVISION;
  buf[3] = contents_len;
  
  if (response->kind == PK_ECHO) {
    memcpy(&buf[4], response->contents.echo.message, 5);
  } else if (response->kind == PK_READ_PARAMETER) {
    uint8_t const parmkind = response->contents.read_parameter.param_kind;
    buf[4] = response->contents.read_parameter.parameter >> 8;
    buf[5] = response->contents.read_parameter.parameter & 0xFF;
    buf[6] = parmkind;
    if (parmkind == PKIND_INT32) {
      buf[7] = (response->contents.read_parameter.int_val >> 24) & 0xFF;
      buf[8] = (response->contents.read_parameter.int_val >> 16) & 0xFF;
      buf[9] = (response->contents.read_parameter.int_val >> 8) & 0xFF;
      buf[10] = response->contents.read_parameter.int_val & 0xFF;
    } else if (parmkind == PKIND_BOOL) {
      buf[7] = response->contents.read_parameter.bool_val;
    } else if (parmkind == PKIND_BYTES) {
      memcpy(buf + 7, response->contents.read_parameter.bytes_val, response->contents_len - 3);
    }
  } else if (response->kind == PK_WRITE_PARAMETER) {
    buf[4] = response->contents.write_parameter.parameter >> 8;
    buf[5] = response->contents.write_parameter.parameter & 0xFF;
    buf[6] = response->contents.write_parameter.param_kind;
  } else if (response->kind == PK_PRINT) {
    int i;
    for (i = 0; i < contents_len - 1; ++i)
      buf[4 + i] = response->contents.print.str[i];
    buf[4 + i] = '\0';
  }
  
  return (error_code) ? error_code : alloc_len;
}


int packet_encode_request(uint8_t *buf, RequestPacket const *const request)
{
  if ((!buf) || (!request))
    return -1;
  
  int error_code = 0;
  
  // calculate contents length
  uint8_t contents_len = 0;
  if (request->kind == PK_ECHO) {
    contents_len = 5;
  } else if (request->kind == PK_READ_PARAMETER) {
    contents_len = 3;
  } else if (request->kind == PK_WRITE_PARAMETER) {
    switch (request->contents.read_parameter.param_kind) {
    case PKIND_INT32: contents_len = 7; break; // add size of int32
    case PKIND_BOOL: contents_len = 4; break;  // add size of bool
    case PKIND_BYTES: contents_len = request->contents_len; break; // bytes has dynamic length, must be set outside this function
    case PKIND_ERR: contents_len = 3; break;   // err packet still has 3 content bytes
    default: error_code = -2; break;  // incorrect value for param kind
    }
  } else
    error_code = -4;
  
  buf[0] = request->kind;
  buf[1] = request->uid;
  buf[2] = PACKET_PROTOCOL_REVISION;
  buf[3] = contents_len;
  
  if (request->kind == PK_ECHO) {
    memcpy(&buf[4], request->contents.echo.message, 5);
  } else if (request->kind == PK_WRITE_PARAMETER) {
    uint8_t const parmkind = request->contents.write_parameter.param_kind;
    buf[4] = request->contents.write_parameter.parameter >> 8;
    buf[5] = request->contents.write_parameter.parameter & 0xFF;
    buf[6] = parmkind;
    if (parmkind == PKIND_INT32) {
      buf[7] = (request->contents.write_parameter.int_val >> 24) & 0xFF;
      buf[8] = (request->contents.write_parameter.int_val >> 16) & 0xFF;
      buf[9] = (request->contents.write_parameter.int_val >> 8) & 0xFF;
      buf[10] = request->contents.write_parameter.int_val & 0xFF;
    } else if (parmkind == PKIND_BOOL) {
      buf[7] = request->contents.write_parameter.bool_val;
    } else if (parmkind == PKIND_BYTES) {
      memcpy(buf + 7, request->contents.write_parameter.bytes_val, contents_len - 3);
    }
  } else if (request->kind == PK_READ_PARAMETER) {
    buf[4] = request->contents.read_parameter.parameter >> 8;
    buf[5] = request->contents.read_parameter.parameter & 0xFF;
    buf[6] = request->contents.read_parameter.param_kind;
  }
  
  return (error_code) ? error_code : contents_len + 4;
}


int packet_decode_response(uint8_t *in, int inlen, ResponsePacket *response)
{
  if ((!response) || (!in))
    return -1; // null ptr
  
  int result = 0;
  
  if (inlen >= 4) {
    uint8_t const pkind = in[0];
    response->kind = pkind;
    response->uid = in[1];
    response->revision = in[2];
    response->contents_len = in[3];
    
    if (inlen >= (4 + response->contents_len)) { // have the full packet
      if (pkind == PK_ECHO) {
        if (response->contents_len == 5) {
          memcpy(response->contents.echo.message, &in[4], response->contents_len);
          result = 9; // set result to number of bytes decoded
        } else
          result = PDQ_BADLEN; // invacontents_lenlid contents_len for an echo packet
      } else if (pkind == PK_READ_PARAMETER) {
        if (response->contents_len < 3)
          result = PDQ_BADLEN; // invalid content_len for any kind of param read
        else {
          uint8_t const parmkind = in[6];
          response->contents.read_parameter.param_kind = parmkind;
          response->contents.read_parameter.parameter = (in[4] << 8) | in[5];
          if ((parmkind == PKIND_INT32) && (response->contents_len == 7)) {
            uint32_t temp = ((uint32_t)in[7]) << 24;
            temp |= ((uint32_t)in[8]) << 16;
            temp |= ((uint32_t)in[9]) << 8;
            temp |= ((uint32_t)in[10]);
            // type-pun temp into a signed value
            response->contents.read_parameter.int_val = *((int32_t*)&temp);
            result = 11; // read int32 is an 11-byte packet
          } else if ((parmkind == PKIND_BOOL) && (response->contents_len == 4)) {
            response->contents.read_parameter.bool_val = in[7];
            result = 8; // read bool is an 8-byte packet
          } else if ((parmkind == PKIND_ERR) && (response->contents_len == 3)) {
            result = 7; // read err is a 7-byte packet
          } else if ((parmkind == PKIND_BYTES) && (response->contents_len <= 19)) {
            memcpy(response->contents.read_parameter.bytes_val, in + 7, response->contents_len - 3);
            result = 4 + response->contents_len;
          } else if ((parmkind == PKIND_BYTES) || (parmkind == PKIND_INT32) || (parmkind == PKIND_BOOL) || (parmkind == PKIND_ERR)) {
            // the param kind is valid, so the contents_len must be wrong
            result = PDQ_BADLEN;
          } else {
            result = PDQ_RWBADPKIND; // the param_kind is invalid
          }
        }
      } else if (pkind == PK_WRITE_PARAMETER) {
        if (response->contents_len == 3) {
          response->contents.write_parameter.parameter = (in[4] << 8) | in[5];
          uint8_t const parmkind = in[6];
          response->contents.write_parameter.param_kind = parmkind;
          if ((parmkind != PKIND_INT32)
              && (parmkind != PKIND_BOOL)
              && (parmkind != PKIND_ERR)
              && (parmkind != PKIND_BYTES))
            result = PDQ_RWBADPKIND; // invalid param_kind
          else
            result = 7;  // write param's response is a 7-byte packet
        } else {
          result = PDQ_BADLEN;  // invalid content_len for a param write
        }
      } else if (pkind == PK_PRINT) {
        response->contents.print.str = (char*)(in + 4);
        result = 4 + response->contents_len;
      } else if (response->revision > PACKET_PROTOCOL_REVISION) {
        result = PDQ_OLDPROTOCOL;  // device's protocol is too new for this software
      } else
        result = PDQ_BADKIND; // invalid response kind
    }
  }
  
  return result;
}
