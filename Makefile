all: firmware

libraries: cmsis hal_stm32f7 stm32


firmware: fwobjects
	

fwobjects:
	@make -C./fw
	@make -f Makefile.2

cmsis:
	@make -C./lib/hal/CMSIS

hal_stm32f7:
	@make -C./lib/hal/STM32F7xx_HAL_Driver

stm32:
	@make -C./lib/stm32

clean:
	@make clean -C./fw

cleanlib:
	@make clean -C./lib/hal/CMSIS
	@make clean -C./lib/hal/STM32F7xx_HAL_Driver
	@make clean -C./lib/stm32
