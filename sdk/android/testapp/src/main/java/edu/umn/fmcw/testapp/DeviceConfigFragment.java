package edu.umn.fmcw.testapp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;

import edu.umn.fmcw.FmcwDevice;

public class DeviceConfigFragment extends Fragment {
  FmcwDevice device;
  public DeviceConfigFragment() {
    Log.d("edu.umn.fmcw.testapp", "DeviceConfigFragment made");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    return inflater.inflate(R.layout.fragment_device_config, container, false);
  }

  @Override
  public void onViewCreated(View view, Bundle savedInstanceState) {
    DeviceActivity activity = (DeviceActivity) getActivity();
    device = activity.device;
    ((TextView)view.findViewById(R.id.config_device_name)).setText(device.getProductName());
    ((TextView)view.findViewById(R.id.config_device_vendor)).setText(device.getManufacturerName());
    ((TextView)view.findViewById(R.id.config_device_kind)).setText(device.getDeviceKind().toString());
//    device.open(
//      (s, __) -> {
//        if (s) {
          device.getSerialNumber(
            (didSucceed, serialNumber) -> {
              getActivity().runOnUiThread(() -> {
                if (didSucceed)
                  ((TextView)view.findViewById(R.id.config_serial_num)).setText(serialNumber);
                else
                  Toast.makeText(getContext(), "failed to get serial number", Toast.LENGTH_SHORT).show();
              });
            }
          );
//        }
//      }
//    );

    view.findViewById(R.id.config_echo_button).setOnClickListener(v -> {
      device.echo(
        (didSucceed, message) -> {
          getActivity().runOnUiThread(
            () -> {
              if (didSucceed) {
                Snackbar.make(v, getString(R.string.echo_test_success) + "(" + (message != null ? message : "null") + ")", Snackbar.LENGTH_SHORT)
                  .show();
              } else {
                Snackbar.make(v, getString(R.string.echo_test_fail) + "(" + (message != null ? message : "null") + ")", Snackbar.LENGTH_LONG)
                  .show();
            }}
          );
        }
      );
    });
  }

  @Override
  public void onResume() {
    super.onResume();
    DeviceActivity activity = (DeviceActivity) getActivity();
    device = activity.device;
  }

  public static DeviceConfigFragment createInstance() {
    DeviceConfigFragment result = new DeviceConfigFragment();
    return result;
  }
}
