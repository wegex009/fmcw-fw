package edu.umn.fmcw.testapp;

import android.content.Context;
import android.content.Intent;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import edu.umn.fmcw.Fmcw;
import edu.umn.fmcw.FmcwDevice;
import edu.umn.fmcw.FmcwSimulatedDevice;
import edu.umn.fmcw.FmcwUsbSerialDevice;
import edu.umn.fmcw.Util;

import static edu.umn.fmcw.testapp.DeviceActivity.EXTRA_FOUND_DEVICE;

public class MainActivity extends AppCompatActivity {

  RecyclerView deviceListView;
  RecyclerView.Adapter deviceListAdapter;
  RecyclerView.LayoutManager deviceListLayoutManager;

  List<FmcwDevice.FoundDevice> devices;

  static final class DeviceListAdapter extends RecyclerView.Adapter<DeviceListAdapter.DeviceListViewHolder> {
    private List<FmcwDevice.FoundDevice> deviceList;

    static final class DeviceListViewHolder extends RecyclerView.ViewHolder {
      public TextView manufacturerName;
      public TextView productName;
      public ImageView deviceIcon;

      public DeviceListViewHolder(View itemView) {
        super(itemView);
        manufacturerName = itemView.findViewById(R.id.manufacturer_name);
        productName = itemView.findViewById(R.id.product_name);
        deviceIcon = itemView.findViewById(R.id.device_icon);
      }
    }

    public DeviceListAdapter(List<FmcwDevice.FoundDevice> deviceList) {
      this.deviceList = deviceList;
    }

    @NonNull
    @Override
    public DeviceListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
      View outv = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.device_list_element, parent, false);
      outv.setOnClickListener(
        view -> {
          int i = parent.indexOfChild(view);
          FmcwDevice.FoundDevice fd = deviceList.get(i);
          Intent intent = new Intent(parent.getContext(), DeviceActivity.class);
          intent.putExtra(EXTRA_FOUND_DEVICE, fd);
          parent.getContext().startActivity(intent);
        }
      );
      return new DeviceListViewHolder(outv);
    }

    @Override
    public void onBindViewHolder(@NonNull DeviceListViewHolder holder, int position) {
      FmcwDevice.FoundDevice fd = deviceList.get(position);
      holder.manufacturerName.setText(fd.getManufacturerName());
      holder.productName.setText(fd.getProductName());
      holder.deviceIcon.setImageResource(fd.getDeviceKind().getDrawableId());
    }

    @Override
    public int getItemCount() {
      return deviceList.size();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    Toolbar toolbar = findViewById(R.id.toolbar);
    toolbar.setTitle(R.string.activity_main_title);
    setSupportActionBar(toolbar);

    Fmcw.createSingleton(this);

    devices = new ArrayList<>();

    deviceListView = findViewById(R.id.device_list);
    deviceListLayoutManager = new LinearLayoutManager(this);
    deviceListView.setLayoutManager(deviceListLayoutManager);
    deviceListAdapter = new DeviceListAdapter(devices);
    deviceListView.setAdapter(deviceListAdapter);

    FloatingActionButton fab = findViewById(R.id.fab);
    fab.setOnClickListener(view -> {
      refreshDeviceList();
    });
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshDeviceList();
  }


  @Override
  protected void onPause() {
    super.onPause();
  }

  private void refreshDeviceList() {
    devices.clear();
    devices.addAll(Util.findDevices(this));
    devices.add(new FmcwSimulatedDevice.FoundSimulatedDevice(this));
    deviceListAdapter.notifyDataSetChanged();
  }
}
