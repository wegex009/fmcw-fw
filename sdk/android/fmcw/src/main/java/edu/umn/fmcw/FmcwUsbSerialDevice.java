package edu.umn.fmcw;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.hoho.android.usbserial.driver.CdcAcmSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialPort;
import com.hoho.android.usbserial.util.SerialInputOutputManager;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FmcwUsbSerialDevice implements FmcwDevice {
  public static final String ACTION_USB_PERMISSION =  "edu.umn.fmcw.USB_PERMISSION";

  public static final int portWriteTimeoutMs = 100;

  // USB serial usbDevice's FoundDevice Implementation
  public static class FoundUsbSerialDevice implements FmcwDevice.FoundDevice {
    private UsbDevice usbDevice;
    private Context context;
    private UsbSerialDriver driver;
    private UsbManager manager;

    FoundUsbSerialDevice(Context context, UsbManager manager, UsbSerialDriver driver) {
      this.usbDevice = driver.getDevice();
      this.context = context;
      this.driver = driver;
      this.manager = manager;
    }

    public Context getContext() { return context; }

    @Override
    public String getManufacturerName() {
      if (Build.VERSION.SDK_INT >= 21)
        return usbDevice.getManufacturerName() != null ?
          usbDevice.getManufacturerName() : context.getString(android.R.string.unknownName);
      else
        return context.getString(android.R.string.unknownName);
    }

    @Override
    public String getProductName() {
      if (Build.VERSION.SDK_INT >= 21)
        return usbDevice.getProductName() != null ?
          usbDevice.getProductName() : context.getString(android.R.string.unknownName);
      else
        return context.getString(android.R.string.unknownName);
    }

    public FmcwDevice createDevice() {
      return new FmcwUsbSerialDevice(this);
    }

    public UsbDevice getUsbDevice() { return usbDevice; }

    @Override
    public DeviceKind getDeviceKind() {
      return DeviceKind.USB_SERIAL;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeParcelable(usbDevice, 0);
    }

    public static final Parcelable.Creator<FoundUsbSerialDevice> CREATOR =
      new Parcelable.Creator<FoundUsbSerialDevice>() {
        @Override
        public FoundUsbSerialDevice createFromParcel(Parcel source) {
          UsbDevice device = source.readParcelable(UsbDevice.class.getClassLoader());
          return new FoundUsbSerialDevice(device, Fmcw.getSingleton().getApplicationContext());
        }

        @Override
        public FoundUsbSerialDevice[] newArray(int size) {
          return new FoundUsbSerialDevice[size];
        }
      };

    private FoundUsbSerialDevice(UsbDevice usbDevice, Context context) {
      this.usbDevice = usbDevice;
      this.context = context;
      driver = new CdcAcmSerialDriver(usbDevice);
      manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
    }
  }

  /* listener and executor for listening to the USB port */

  private final SerialInputOutputManager.Listener listener =
    new SerialInputOutputManager.Listener() {
      @Override
      public void onNewData(byte[] data) {
        protocol.recv(data);
      }

      @Override
      public void onRunError(Exception e) {

      }
    };

  private final ExecutorService executorService = Executors.newSingleThreadExecutor();

  /* broadcast receiver for getting USB permissions */

  private final class UsbPermissionReceiver extends BroadcastReceiver {
    OpenCompletion completion;
    @Override
    public void onReceive(Context context, Intent intent) {
      context.unregisterReceiver(this);
      Exception e = null;
      String action = intent.getAction();
      if (ACTION_USB_PERMISSION.equals(action)) {
        synchronized (this) {
          UsbDevice d = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

          if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
            if (d != null) {
              //set up device communication
              connection = manager.openDevice(d);
              port = driver.getPorts().get(0);
              try {
                port.open(connection);
                serialIoManager = new SerialInputOutputManager(port, listener);
                executorService.submit(serialIoManager);
                open = true;
              } catch (IOException ee) {
                e = ee;
              }
            }
          } else {
            e = new SecurityException("User did not grant permission to use this device");
          }
        }
      }

      if (completion != null)
        completion.exec(open, e);
    }
  }

  /* packet protocol implementation */
  private final PacketProtocol protocol =
    new PacketProtocol() {
      @Override
      void send(byte[] bytes) {
        try {
          port.write(bytes, portWriteTimeoutMs);
        } catch (IOException e) {
          Log.e(loggerPrefix, constructDebugString("Exception occurred while sending bytes (IOException)"), e.getCause());
          close(null);
        } catch (NullPointerException e) {
          Log.e(loggerPrefix, constructDebugString("Exception occurred while sending bytes (NullPointerException)"), e.getCause());
          close(null);
        }
      }

      @Override
      void gotEchoResponse(EchoRequest request, EchoResponse response, ResponsePacket fullResponse, Object associatedObj) {
        if (associatedObj != null)
          ((EchoCompletion)associatedObj).exec(true, response.getMessage());
      }

      @Override
      void gotWriteParameterResponse(WriteRequest request, WriteResponse response, ResponsePacket fullResponse, Object associatedObj) {
        if (response.getParamKind() == ParamKind.ERR) {
          Log.w(loggerPrefix, constructDebugString("Error when writing parameter: request(" +
            "kind=" + request.getParamKind() +
            ", param=" + request.getParameter() +
            ", protocol_revision=" + PacketProtocol.getPacketProtocolRevision() +
            ") response(" +
            "kind=" + response.getParamKind() +
            ", param=" + response.getParameter() +
            ", protocol_revision=" + fullResponse.getRevisionNumber()));
        } else {
          Log.d(loggerPrefix, constructDebugString("Successful write to " + response.getParameter()));
        }
      }

      @Override
      void gotReadParameterResponse(ReadRequest request, ReadResponse response, ResponsePacket fullResponse, Object associatedObj) {
        if (response.getParamKind() == ParamKind.ERR) {
          Log.w(loggerPrefix, constructDebugString("Error when reading parameter: response(" +
            "kind=" + response.getParamKind() +
            ", param=" + response.getParameter() +
            ", protocol_revision=" + fullResponse.getRevisionNumber()));
          if (associatedObj != null) {
            if (associatedObj instanceof GetSerialNumberCompletion)
              ((GetSerialNumberCompletion) associatedObj).exec(false, null);
          }
        } else if (response.getParamKind() == ParamKind.INT32) {
          Log.d(loggerPrefix, constructDebugString("Read of param #" + response.getParameter() +
            " yielded " + ((ReadIntResponse)response).getValue()));
//          if (associatedObj != null) { // handle completions for read int
//          }
        } else if (response.getParamKind() == ParamKind.BOOL) {
          Log.d(loggerPrefix, constructDebugString("Read of param #" + response.getParameter() +
            " yielded " + ((ReadBoolResponse) response).getValue()));
//          if (associatedObj != null) { // handle completions for read bool
//          }
        } else if (response.getParamKind() == ParamKind.BYTES) {
          String byteString = Util.bytesToHexString(((ReadBytesResponse) response).getValue(), false);
          Log.d(loggerPrefix, constructDebugString("Read of param #" + response.getParameter() +
            " yielded " + byteString));
          if (associatedObj != null) { // handle completions for read bytes
            if (associatedObj instanceof GetSerialNumberCompletion) // serial number
              ((GetSerialNumberCompletion) associatedObj).exec(true, byteString);
            else if (associatedObj instanceof ScanCompletion) {     // scan
              byte[] respBytes = ((ReadBytesResponse) response).getValue();
              Util.assertion(respBytes.length == 16, "scan response wrong length");
              float[] resp = new float[4];
              for (int i = 0; i < resp.length; ++i) {
                resp[i] = Float.intBitsToFloat(
                  (respBytes[i * 4] << 24) +
                  (respBytes[(i * 4) + 1] << 16) +
                  (respBytes[(i * 4) + 2] << 8) +
                  (respBytes[(i * 4) + 3])
                );
              }
              ((ScanCompletion) associatedObj).exec(true, resp, null);
            }
          }
        }
      }

      @Override
      void gotPrintResponse(String message, ResponsePacket fullResponse) {
        Log.d(loggerPrefix, constructDebugString(message));
      }
    };

  /* object variables, getters, and setters */

  private Context context;
  private UsbDevice usbDevice;
  private UsbManager manager;
  private UsbSerialPort port;
  private UsbDeviceConnection connection;
  private boolean open;
  private boolean permissionsRequested;
  private SerialInputOutputManager serialIoManager;
  private UsbSerialDriver driver;
  private boolean scanInProgress;

  public Context getContext() { return context; }
  public UsbDevice getUsbDevice() { return usbDevice; }
  public boolean isOpen() { return open; }

  /* constructors */

  private FmcwUsbSerialDevice(FoundUsbSerialDevice foundDevice) {
    context = foundDevice.context;
    usbDevice = foundDevice.usbDevice;
    driver = foundDevice.driver;
    manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
    open = false;
    permissionsRequested = false;
    serialIoManager = null;
    scanInProgress = false;
  }

  public FmcwUsbSerialDevice(Context context, UsbDevice device) {
    this.context = context;
    usbDevice = device;
    driver = new CdcAcmSerialDriver(device);
    manager = (UsbManager)context.getSystemService(Context.USB_SERVICE);
    open = false;
    permissionsRequested = false;
    serialIoManager = null;
    scanInProgress = false;
  }

  public void open(OpenCompletion completion) {
    Exception ce = null;

    if (!permissionsRequested) {
      final PendingIntent permissionIntent =
        PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
      IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
      UsbPermissionReceiver receiver = new UsbPermissionReceiver();
      receiver.completion = completion;
      context.registerReceiver(receiver, filter);
      permissionsRequested = true;
      manager.requestPermission(usbDevice, permissionIntent);
    } else {
      try {
        port.open(connection);
        open = true;
      } catch (IOException e) {
        Log.e(loggerPrefix, "FmcwUsbSerialDevice failed to open (IOException)", e.getCause());
        ce = e;
      } catch (NullPointerException pe) {
        Log.e(loggerPrefix, "FmcwUsbSerialDevice failed to open (NullPointerException)", pe.getCause());
        ce = pe;
      } finally {
        if (open) {
          serialIoManager = new SerialInputOutputManager(port, listener);
          executorService.submit(serialIoManager);
        }
      }
      if (completion != null)
        completion.exec(open, ce);
    }
  }

  public void close(CloseCompletion completion) {
    Exception ce = null;
    if (open) {
      serialIoManager.stop();
      serialIoManager = null;
      try {
        port.close();
        open = false;
      } catch (IOException e) {
        Log.e(loggerPrefix, "FmcwUsbSerialDevice failed to close (IOException)", e.getCause());
        ce = e;
      } catch (NullPointerException pe) {
        // caused by user not letting us access the device
        Log.e(loggerPrefix, "FmcwUsbSerialDevice failed to open (NullPointerException)", pe.getCause());
        ce = pe;
      }
    }

    if (completion != null)
      completion.exec(!open, ce);
  }

  @Override
  public void echo(EchoCompletion completion) {
    protocol.sendEchoRequest(completion);
  }

  @Override
  public synchronized void scan(ScanCompletion completion) {
    if (!scanInProgress) {
      scanInProgress = true;
      protocol.sendReadRequest(PacketProtocol.ParamKind.BYTES, 0x101, completion);
    }
  }

  @Override
  public String getManufacturerName() {
    if (Build.VERSION.SDK_INT >= 21)
      return usbDevice.getManufacturerName() != null ?
        usbDevice.getManufacturerName() : context.getString(android.R.string.unknownName);
    else
      return context.getString(android.R.string.unknownName);
  }

  @Override
  public String getProductName() {
    if (Build.VERSION.SDK_INT >= 21)
      return usbDevice.getProductName() != null ?
        usbDevice.getProductName() : context.getString(android.R.string.unknownName);
    else
      return context.getString(android.R.string.unknownName);
  }

  @Override
  public DeviceKind getDeviceKind() {
    return DeviceKind.USB_SERIAL;
  }

  @Override
  public void getSerialNumber(GetSerialNumberCompletion completion) {
    protocol.sendReadRequest(PacketProtocol.ParamKind.BYTES, 0x100, completion);
  }

  private String constructDebugString(String message) {
    if (Build.VERSION.SDK_INT >= 21) {
      String manufacturerName = usbDevice.getManufacturerName() != null ?
        usbDevice.getManufacturerName() :
        String.valueOf(usbDevice.getProductId());
      String productName = usbDevice.getProductName() != null ?
        usbDevice.getProductName() :
        String.valueOf(usbDevice.getProductId());
      return manufacturerName + "/" + productName + ":" + message;
    } else {
      String manufacturerName = String.valueOf(usbDevice.getProductId());
      String productName = String.valueOf(usbDevice.getProductId());
      return manufacturerName + "/" + productName + ":" + message;
    }
  }
}
