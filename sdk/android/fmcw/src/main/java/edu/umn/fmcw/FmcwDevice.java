package edu.umn.fmcw;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.IOException;

/**
 * The common interface used by all FmcwDevices.
 */
public interface FmcwDevice {
  public static final String loggerPrefix = "edu.umn.fmcw";

  Context getContext();
  boolean isOpen();

  interface OpenCompletion {
    void exec(boolean didSucceed, Exception e);
  }

  /**
   * Open a connection to the FmcwDevice.
   * @param completion runs when the attempt to open a connection is completed.
   */
  void open(OpenCompletion completion);


  interface CloseCompletion {
    void exec(boolean didSucceed, Exception e);
  }

  /**
   * Close the connection to the FmcwDevice.
   * @param completion runs when the attempt to close the connection is completed.
   */
  void close(CloseCompletion completion);


  interface EchoCompletion {
    void exec(boolean didSucceed, String message);
  }

  /**
   * Ping the device.
   * @param completion runs when the device responds to the ping, or after the
   *                   ping times out.
   */
  void echo(EchoCompletion completion);

  interface GetSerialNumberCompletion {
    void exec(boolean didSucceed, String serialNumber);
  }

  /**
   * Get the device's serial number.
   * @param completion runs when the serial number has been received, or the request failed.
   */
  void getSerialNumber(GetSerialNumberCompletion completion);

  // TODO: make the scan completion return actual object distances instead of peak frequencies
  interface ScanCompletion {
    void exec(boolean didSucceed, float[] peakFrequencies, String message);
  }

  /**
   * Perform a single FMCW scan.
   * @param completion runs when the device responds to the scan request, or runs immediately
   *                   if there is already a scan in progress.
   */
  void scan(ScanCompletion completion);

  String getManufacturerName();

  String getProductName();

  DeviceKind getDeviceKind();


  /**
   * Represents different kinds of FMCW Devices. Instances of the
   * FoundDevice class have a method called getDeviceKind, which returns one
   * of the enum values here. {@link DeviceKind#getAssociatedClass()} returns the
   * class that {@link FoundDevice#createDevice()} will return when it is called
   * on this particular FoundDevice.
   *
   * <code>
   *  static void foo(FoundDevice fd) {
   *     if (fd.getDeviceKind() == DeviceKind.USB_SERIAL) {
   *       assert fd.getDeviceKind().getAssociatedClass() == FmcwUsbSerialDevice.class;
   *       // it is safe to cast fd.createDevice() to FmcwUsbSerialDevice.
   *       FmcwUsbSerialDevice device = (FmcwUsbSerialDevice) fd.createDevice();
   *     }
   *   }
   * </code>
   *
   * @see FoundDevice#getDeviceKind()
   */
  enum DeviceKind {
    USB_SERIAL(FmcwUsbSerialDevice.class, R.string.device_kind_usb_serial, R.drawable.usb_64, 0),
    SIMULATOR(FmcwSimulatedDevice.class, R.string.device_kind_simulator, R.drawable.simulator_64, 1);

    private Class clazz;
    private int textId;
    private int drawableId;
    private int intVal;

    public Class getAssociatedClass() { return clazz; }
    public String getLocalizedName(Context context) {
      return context.getString(textId);
    }
    public int getDrawableId() {
      return drawableId;
    }
    public int getIntVal() {
      return intVal;
    }

    DeviceKind(Class clazz, int textId, int drawableId, int intVal) {
      this.clazz = clazz;
      this.textId = textId;
      this.drawableId = drawableId;
      this.intVal = intVal;
    }

    public static DeviceKind fromInteger(int val) throws IllegalArgumentException {
      switch (val) {
        case 0: return USB_SERIAL;
        case 1: return SIMULATOR;
        default: throw new IllegalArgumentException("given value does not correspond to a DeviceKind");
      }
    }
  }

  interface FoundDevice extends Parcelable {
    /**
     * @return a new FmcwDevice, which allows you to start interacting with the device.
     */
    FmcwDevice createDevice();

    /**
     * @return a DeviceKind signifying the type of device this FoundDevice represents.
     *         Currently, the only device kind is USB_SERIAL.
     */
    DeviceKind getDeviceKind();

    /**
     * @return the Context under which this FoundDevice was instantiated.
     */
    Context getContext();

    String getManufacturerName();

    String getProductName();
  }
}
