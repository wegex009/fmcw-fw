package edu.umn.fmcw;

import android.util.Log;
import android.util.Pair;

// Static members the JNI bindings for the packet protocol
// Also contains classes describing request and response packets.

/**
 * Handles packet serialization and deserialization for FmcwDevices.
 * Provides send...Request() methods, which automatically assign uids to
 * the packets, then call the implemented recv() method, which must send
 * the bytes to the physical device.
 * Buffers received bytes, and calls the got...Response(...) methods when
 * a response is received for a particular request.
 */
public abstract class PacketProtocol {
  static {
    System.loadLibrary("bridge");
  }

  private static final String loggerPrefix = "edu.umn.fmcw";


  private byte[] buffer;
  private int nbytes;
  private RequestPacket[] activeRequests;
  private Object[] associatedObjects;
  private int nextUid;


  public PacketProtocol() {
    buffer = new byte[256];
    nbytes = 0;
    activeRequests = new RequestPacket[256];
    associatedObjects = new Object[256];
    nextUid = 0;
  }

  public byte[] getBuffer() {
    byte[] result = new byte[nbytes];
    return buffer.clone();
  }

  abstract void send(byte[] bytes);
  abstract void gotEchoResponse(EchoRequest request, EchoResponse response, ResponsePacket fullResponse, Object associatedObj);
  abstract void gotWriteParameterResponse(WriteRequest request, WriteResponse response, ResponsePacket fullResponse, Object associatedObj);
  abstract void gotReadParameterResponse(ReadRequest request, ReadResponse response, ResponsePacket fullResponse, Object associatedObj);
  abstract void gotPrintResponse(String message, ResponsePacket fullResponse);

  public synchronized int sendEchoRequest(Object assocatedObj) {
    EchoRequest request = new EchoRequest();
    int result = addToRequestTable(request, assocatedObj);
    if (result >= 0 && result < 256) {
      Log.d(loggerPrefix, "sending echo request with uid " + result);
      request.uid = result;
      send(request.encode());
    } else {
      Log.e(loggerPrefix, "send: erroneous uid " + result);
    }
    return result;
  }

  public synchronized int sendWriteRequest(int parameter, int value, Object associatedObj) {
    WriteIntRequest request = new WriteIntRequest(parameter, value);
    int result = addToRequestTable(request, associatedObj);
    if (result >= 0 && result < 256) {
      request.uid = result;
      send(request.encode());
    }
    return result;
  }

  public synchronized int sendWriteRequest(int parameter, boolean value, Object associatedObj) {
    WriteBoolRequest request = new WriteBoolRequest(parameter, value);
    int result = addToRequestTable(request, associatedObj);
    if (result >= 0 && result < 256) {
      request.uid = result;
      send(request.encode());
    }
    return result;
  }

  public synchronized int sendReadRequest(ParamKind paramKind, int parameter, Object associatedObj) {
    ReadRequest request = new ReadRequest(parameter, paramKind);
    int result = addToRequestTable(request, associatedObj);
    if (result >= 0 && result < 256) {
      request.uid = result;
      send(request.encode());
    }
    return result;
  }

  /**
   * Buffer new bytes, and try to process a new packet.
   * @param bytes the new bytes to buffer.
   */
  synchronized void recv(byte[] bytes) {
    byte[] tmp = buffer;
    // if the buffer is too small, create a temp buffer that's big enough to hold all the new bytes
    while (bytes.length + nbytes > tmp.length) {
      tmp = new byte[tmp.length * 2];
    }
    // if we had to make a new buffer to hold the new bytes, copy the old buffer into the new one
    // then set `buffer` to the new, longer buffer
    if (tmp != buffer) {
      if (nbytes >= 0)
        System.arraycopy(buffer, 0, tmp, 0, nbytes);
      buffer = tmp;
    }
    // copy the new bytes into the buffer.
    System.arraycopy(bytes, 0, buffer, nbytes, bytes.length);
    nbytes += bytes.length;

    Log.d(loggerPrefix, "CURRENT BUFFER: " + Util.bytesToPrettyHexString(buffer, false));
    Log.d(loggerPrefix, "BUFLEN: " + nbytes);

    if (nbytes > 4) { // all packets are at least 4 bytes, so don't bother till we have that
      ResponsePacket rp = new ResponsePacket();
      int ndecoded = rp.decode(buffer, nbytes);
      if (ndecoded > 0) {
        // shift buffer forward
        for (int i = ndecoded; i < nbytes; ++i)
          buffer[i - ndecoded] = buffer[i];
        nbytes -= ndecoded;
        handleResponse(rp);
      } else if (ndecoded < 0) {
        if (rp.getContentsLen() > 0) {
          Log.e(loggerPrefix, "Decoding a response caused error code + (" + ndecoded + ")");
          for (int i = 4 + rp.getContentsLen(); i < nbytes; ++i)
            buffer[i - (4 + rp.getContentsLen())] = buffer[i];
        } else {
          Util.assertion(false, "Packet protocol went boom (" + ndecoded + ")");
        }
      }
    }
  }

  private synchronized void handleResponse(ResponsePacket rp) {
    if (rp.getKind() == PacketKind.ECHO) {
      Log.d(loggerPrefix, "handling echo respnse for uid " + rp.uid);
      Pair<RequestPacket, Object> reqdata = removeFromRequestTable(rp.uid);
      EchoResponse responseBody = ((EchoResponse)rp.getResponseBody());
      gotEchoResponse((EchoRequest)reqdata.first, responseBody, rp, reqdata.second);
    } else if (rp.getKind() == PacketKind.READ_PARAMETER) {
      Pair<RequestPacket, Object> reqdata = removeFromRequestTable(rp.uid);
      ReadResponse responseBody = ((ReadResponse) rp.getResponseBody());
      gotReadParameterResponse((ReadRequest)reqdata.first, responseBody, rp, reqdata.second);
    } else if (rp.getKind() == PacketKind.WRITE_PARAMETER) {
      Pair<RequestPacket, Object> reqdata = removeFromRequestTable(rp.uid);
      WriteResponse responseBody = ((WriteResponse)rp.getResponseBody());
      gotWriteParameterResponse((WriteRequest)reqdata.first, responseBody, rp, reqdata.second);
    } else if (rp.getKind() == PacketKind.PRINT) {
      gotPrintResponse(((PrintResponse)rp.responseBody).message, rp);
    }
  }

  private synchronized int addToRequestTable(RequestPacket requestPacket, Object associatedObj) {
    int result = nextUid;
    int next = (result + 1) % 256;
    while ((activeRequests[result] != null) && (next != nextUid)) {
      result = next;
      next = (result + 1) % 256;
    }
    Util.assertion(activeRequests[result] == null,
      "out of uids");
    nextUid = next; // increment nextUid if taking an open slot at result
    activeRequests[result] = requestPacket;
    associatedObjects[result] = associatedObj;
    requestPacket.uid = result;
    return result;
  }

  private synchronized Pair<RequestPacket, Object> removeFromRequestTable(int uid) {
    Pair<RequestPacket, Object> result = new Pair<>(
      activeRequests[uid],
      associatedObjects[uid]
    );
    activeRequests[uid] = null;
    associatedObjects[uid] = null;
    return result;
  }

  private synchronized Pair<RequestPacket, Object> peekRequestTable(int uid) {
    Pair<RequestPacket, Object> result = new Pair<>(
      activeRequests[uid],
      associatedObjects[uid]
    );
    return result;
  }


  /* BEGIN PACKET SERIALIZATION STUFF */

  public enum PacketKind {
    ECHO((byte)0),
    WRITE_PARAMETER((byte)1),
    READ_PARAMETER((byte)2),
    PRINT((byte)3);
    private byte num;
    public byte getNum() { return num; }
    PacketKind(byte num) {
      this.num = num;
    }
    static PacketKind fromByte(byte num) {
      switch (num) {
        case 0: return ECHO;
        case 1: return WRITE_PARAMETER;
        case 2: return READ_PARAMETER;
        case 3: return PRINT;
        default: return null;
      }
    }
  }

  enum ParamKind {
    INT32((byte)1),
    BOOL((byte)2),
    BYTES((byte)3),
    ERR((byte)-1);
    private byte num;
    public byte getNum() { return num; }
    ParamKind(byte num) { this.num = num; }
    static ParamKind fromByte(byte num) {
      switch (num) {
        case 1: return INT32;
        case 2: return BOOL;
        case 3: return BYTES;
        case -1: return ERR;
        default: return null;
      }
    }
  }

  /* response packets */

  public static class ResponsePacket {
    private byte kind;  // set by JNI
    private byte uid;  // set by JNI
    private byte revisionNumber;  // set by JNI
    private byte contentsLen;  // set by JNI
    ResponseBody responseBody;
    public PacketKind getKind() { return PacketKind.fromByte(kind); }
    public byte getUid() { return uid; }
    public byte getRevisionNumber() { return revisionNumber; }
    public byte getContentsLen() { return contentsLen; }
    public ResponseBody getResponseBody() { return responseBody; }

    public int decode(byte[] arr, int length) {
      return decodeResponse(this, arr, length);
    }

    // these methods are called from inside the JNI once it has filled the
    // kind, uid, revisionNumber, and contentsLen fields.
    private void jniSetEchoBody(String message) {
      Log.d(loggerPrefix, "jniSetEchoBody(" + message + ")");
      responseBody = new EchoResponse(message);
    }

    private void jniSetReadIntBody(int parameter, int paramKind, int value) {
      Log.d(loggerPrefix, "jniSetReadIntBody(" + parameter + ", " + paramKind + ", " + value + ")");
      if (!(paramKind == ParamKind.INT32.getNum()))
        throw new AssertionError("paramKind == ParamKind.INT32.getNum()");
      responseBody = new ReadIntResponse(parameter, paramKind, value);
    }

    private void jniSetReadBoolBody(int parameter, int paramKind, boolean value) {
      Log.d(loggerPrefix, "jniSetReadBoolBody(" + parameter + ", " + paramKind + ", " + value + ")");
      if (!(paramKind == ParamKind.BOOL.getNum()))
        throw new AssertionError("paramKind == ParamKind.BOOL.getNum()");
      responseBody = new ReadBoolResponse(parameter, paramKind, value);
    }

    private void jniSetReadBytesBody(int parameter, int paramKind, byte[] value) {
      Log.d(loggerPrefix, "jniSetReadBytesBody(" + parameter + ", " + paramKind + ", " + Util.bytesToPrettyHexString(value, false) + ")");
      if (!(paramKind == ParamKind.BYTES.getNum()))
        throw new AssertionError("paramKind == ParamKind.BYTES.getNum()");
      responseBody = new ReadBytesResponse(parameter, paramKind, value);
    }

    private void jniSetReadBody(int parameter, int paramKind) {
      Log.d(loggerPrefix, "jniSetReadBody(" + parameter + ", " + paramKind  + ")");
      responseBody = new ReadResponse(parameter, paramKind);
    }

    private void jniSetWriteBody(int parameter, int paramKind) {
      Log.d(loggerPrefix, "jniSetWriteBody(" + parameter + ", " + paramKind  + ")");
      responseBody = new WriteResponse(parameter, paramKind);
    }

    private void jniPrintPacket(String message) {
      Log.d(loggerPrefix, "jniPrintPacket: " + message);
      responseBody = new PrintResponse(message);
    }
  }

  static abstract class ResponseBody {}

  public static class EchoResponse extends ResponseBody {
    private String message;
    public String getMessage() { return message; }

    public EchoResponse(String message) {
      this.message = message;
    }
  }

  public static class WriteResponse extends ResponseBody {
    private int parameter;
    private ParamKind paramKind;
    public int getParameter() { return parameter; }
    public ParamKind getParamKind() { return paramKind; }

    public WriteResponse(int parameter, int paramKind) {
      this.parameter = parameter;
      if (paramKind >= 0 && paramKind <= 255)
        this.paramKind = ParamKind.fromByte((byte)paramKind);
      else
        this.paramKind = null;
    }
  }

  public static class ReadResponse extends ResponseBody {
    private int parameter;
    private ParamKind paramKind;
    public int getParameter() { return parameter; }
    public ParamKind getParamKind() { return paramKind; }

    public ReadResponse(int parameter, int paramKind) {
      this.parameter = parameter;
      if (paramKind >= 0 && paramKind <= 255)
        this.paramKind = ParamKind.fromByte((byte)paramKind);
      else
        this.paramKind = null;
    }
  }

  public static class ReadIntResponse extends ReadResponse {
    private int value;
    public int getValue() { return value; }

    public ReadIntResponse(int parameter, int paramKind, int value) {
      super(parameter, paramKind);
      this.value = value;
    }
  }

  public static class ReadBoolResponse extends ReadResponse {
    private boolean value;
    public boolean getValue() { return value; }

    public ReadBoolResponse(int parameter, int paramKind, boolean value) {
      super(parameter, paramKind);
      this.value = value;
    }
  }

  public static class ReadBytesResponse extends ReadResponse {
    private byte[] value;
    public byte[] getValue() { return value; }

    public ReadBytesResponse(int parameter, int paramKind, byte[] value) {
      super(parameter, paramKind);
      this.value = value;
    }
  }

  public static class PrintResponse extends ResponseBody {
    String message;

    public PrintResponse(String message) {
      this.message = message;
    }
  }


  /* request packets */

  static abstract class RequestPacket {
    PacketKind kind;
    int uid;
    public PacketKind getKind() { return kind; }

    abstract byte[] encode();
  }

  public static class EchoRequest extends RequestPacket {
    public EchoRequest() {
      kind = PacketKind.ECHO;
    }

    @Override
    public byte[] encode() {
      return encodeEchoRequest(uid);
    }
  }

  public static class ReadRequest extends RequestPacket {
    private int parameter;
    private ParamKind parameter_kind;

    public ReadRequest(int parameter, ParamKind parameter_kind) {
      kind = PacketKind.READ_PARAMETER;
      this.parameter_kind = parameter_kind;
      this.parameter = parameter;
    }

    @Override
    byte[] encode() {
      return encodeReadRequest(uid, parameter, parameter_kind.getNum());
    }
  }

  static abstract class WriteRequest extends RequestPacket {
    abstract ParamKind getParamKind();
    abstract int getParameter();
  }

  public static class WriteIntRequest extends WriteRequest {
    int parameter;
    int value;

    public WriteIntRequest(int parameter, int value) {
      kind = PacketKind.WRITE_PARAMETER;
      this.parameter = parameter;
      this.value = value;
    }

    @Override
    public ParamKind getParamKind() {
      return ParamKind.INT32;
    }

    @Override
    public int getParameter() {
      return parameter;
    }

    @Override
    byte[] encode() {
      return encodeWriteIntRequest(uid, parameter, value);
    }
  }

  public static class WriteBoolRequest extends WriteRequest {
    int parameter;
    boolean value;

    public WriteBoolRequest(int parameter, boolean value) {
      kind = PacketKind.WRITE_PARAMETER;
      this.parameter = parameter;
      this.value = value;
    }

    @Override
    public ParamKind getParamKind() {
      return ParamKind.BOOL;
    }

    @Override
    public int getParameter() {
      return parameter;
    }

    @Override
    byte[] encode() {
      return encodeWriteBoolRequest(uid, parameter, value ? (byte)1 : (byte)0);
    }
  }


  static int getPacketKind(byte[] arr) {
    int result = -1;
    if (arr.length > 0)
      result = arr[0];
    return result;
  }


  private static native byte[] encodeEchoRequest(int uid);
  private static native byte[] encodeWriteIntRequest(int uid, int parameter, int intVal);
  private static native byte[] encodeWriteBoolRequest(int uid, int parameter, byte boolVal);
  private static native byte[] encodeReadRequest(int uid, int parameter, byte paramKind);

  private static native int decodeResponse(ResponsePacket response, byte[] arr, int length);

  public static native int getPacketProtocolRevision();

  /* END PACKET SERIALIZATION STUFF */

}
