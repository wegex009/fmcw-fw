package edu.umn.fmcw;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.util.Pair;

import java.util.Random;

import static java.lang.Float.floatToRawIntBits;

/**
 * This FmcwDevice is a fake one, it simulates a real FmcwDevice. This
 * is mostly useful for debugging stuff.
 */
public class FmcwSimulatedDevice implements FmcwDevice {

  public static class FoundSimulatedDevice implements FmcwDevice.FoundDevice {
    private Context context;
    @Override
    public FmcwDevice createDevice() {
      return new FmcwSimulatedDevice(context);
    }

    @Override
    public FmcwDevice.DeviceKind getDeviceKind() {
      return FmcwDevice.DeviceKind.SIMULATOR;
    }

    @Override
    public Context getContext() {
      return context;
    }

    @Override
    public String getManufacturerName() {
      return "FMCW Radar Team";
    }

    @Override
    public String getProductName() {
      return "Simulator";
    }

    public FoundSimulatedDevice(Context context) {
      this.context = context;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    public static final Parcelable.Creator<FoundSimulatedDevice> CREATOR =
      new Parcelable.Creator<FoundSimulatedDevice>() {
        @Override
        public FoundSimulatedDevice createFromParcel(Parcel source) {
          return new FmcwSimulatedDevice.FoundSimulatedDevice(
            Fmcw.getSingleton().getApplicationContext());
        }

        @Override
        public FoundSimulatedDevice[] newArray(int size) {
          return new FoundSimulatedDevice[size];
        }
      };
  }

  private Context context;
  private boolean open;

  private static float randFloat(float min, float max) {
    Random rand = new Random();
    return (rand.nextFloat() * (max - min)) + min;
  }

  private final PacketProtocol protocol =
    new PacketProtocol() {
      @Override
      void send(byte[] bytes) {
        switch (bytes[0]) {
          case 0: // echo
            byte[] respbytes = {0, bytes[1], bytes[2],
              5, 'e', 'c',  'h', 'o', '\0'
            };
            new Handler().postDelayed(() -> recv(respbytes), 10);
            break;
          case 2: // read
            if (bytes[4] == 0x01 && bytes[5] == 0x00 && bytes[6] == 0x03) { // read serial number
              byte[] respBytes = {2, bytes[1], bytes[2], 15, 0x01, 0x00, 0x03,
                0x01, 0x02, 0x03, 0x04,
                0x05, 0x06, 0x07, 0x08,
                0x09, 0x0A, 0x0B, 0x0C};
              new Handler().postDelayed(() -> recv(respBytes), 20);
            } else if (bytes[4] == 0x01 && bytes[5] == 0x01 && bytes[6] == 0x03) { // scan
              int freqs[] = {
                floatToRawIntBits(randFloat(300000.0f, 1200000.0f)), //TODO: adjust limits of random frequencies
                floatToRawIntBits(randFloat(300000.0f, 1200000.0f)), //TODO: make frequencies behave like device's output
                floatToRawIntBits(randFloat(300000.0f, 1200000.0f)),
                floatToRawIntBits(randFloat(300000.0f, 1200000.0f))
              };
              byte[] respBytes = {2, bytes[1], bytes[2], 19, 0x01, 0x01, 0x03,
                0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00};
              for (int i = 0; i < 4; ++i) {
                respBytes[7 + (i * 4)] = (byte)((freqs[i] >> 24) & 0xFF);
                respBytes[8 + (i * 4)] = (byte)((freqs[i] >> 16) & 0xFF);
                respBytes[9 + (i * 4)] = (byte)((freqs[i] >> 8) & 0xFF);
                respBytes[10 + (i * 4)] = (byte)(freqs[i] & 0xFF);
              }
              new Handler().postDelayed(() -> recv(respBytes), 150);
            }
            break;
          default:
            break;
        }
      }

      @Override
      void gotEchoResponse(EchoRequest request, EchoResponse response, ResponsePacket fullResponse, Object associatedObj) {
        if (associatedObj != null) {
          EchoCompletion completion = (EchoCompletion)associatedObj;
          completion.exec(true, response.getMessage());
        }
      }

      @Override
      void gotWriteParameterResponse(WriteRequest request, WriteResponse response, ResponsePacket fullResponse, Object associatedObj) {

      }

      @Override
      void gotReadParameterResponse(ReadRequest request, ReadResponse response, ResponsePacket fullResponse, Object associatedObj) {
        if (response.getParameter() == 0x100) {
          String serialCode = Util.bytesToHexString(((ReadBytesResponse) response).getValue(), false);
          ((GetSerialNumberCompletion) associatedObj).exec(true, serialCode);
        } else if (response.getParameter() == 0x101) {
          byte[] respBytes = ((ReadBytesResponse) response).getValue();
          Util.assertion(respBytes.length == 16, "scan respBytes.length == 16");

          float[] resp = new float[4];
          for (int i = 0; i < 4; ++i) {
            resp[i] = Float.intBitsToFloat(
              (respBytes[i * 4] << 24) +
              (respBytes[(i * 4) + 1] << 16) +
              (respBytes[(i * 4) + 2] << 8) +
              (respBytes[(i * 4) + 3])
            );
          }

          ((ScanCompletion) associatedObj).exec(true, resp, null);
        } else {
          Util.assertion(false, "failed in param response");
        }
      }

      @Override
      void gotPrintResponse(String message, ResponsePacket fullResponse) {
        Log.d(loggerPrefix, "FmcwSimulatedDevice: " + message);
      }
    };

  public FmcwSimulatedDevice(Context context) {
    this.context = context;
    open = false;
  }

  @Override
  public Context getContext() {
    return context;
  }

  @Override
  public boolean isOpen() {
    sendPrintResponse("already open!");
    return open;
  }

  @Override
  public void open(OpenCompletion completion) {
    open = true;
    if (completion != null)
      completion.exec(true, null);
  }

  @Override
  public void close(CloseCompletion completion) {
    open = false;
    if (completion != null)
      completion.exec(true, null);
  }

  @Override
  public void echo(EchoCompletion completion) {
    protocol.sendEchoRequest(completion);
  }

  @Override
  public void scan(ScanCompletion completion) {
    protocol.sendReadRequest(PacketProtocol.ParamKind.BYTES, 0x101, completion);
  }

  @Override
  public String getManufacturerName() {
    return "FMCW Radar Team";
  }

  @Override
  public String getProductName() {
    return "Simulator";
  }

  @Override
  public DeviceKind getDeviceKind() {
    return DeviceKind.SIMULATOR;
  }

  @Override
  public void getSerialNumber(GetSerialNumberCompletion completion) {
    protocol.sendReadRequest(PacketProtocol.ParamKind.BYTES, 0x100, completion);
  }

  private void sendPrintResponse(String message) {
    byte[] msgBytes = message.getBytes();
    int len = msgBytes.length < 250 ? msgBytes.length : 250;
    byte[] buf = new byte[len + 5];
    buf[0] = PacketProtocol.PacketKind.PRINT.getNum();
    buf[1] = 0;
    buf[2] = (byte)PacketProtocol.getPacketProtocolRevision();
    buf[3] = (byte)(len + 1);
    System.arraycopy(msgBytes, 0, buf, 4, len);
    buf[4 + len] = 0;
    protocol.recv(buf);
  }
}
