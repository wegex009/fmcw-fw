#include <jni.h>
#include <string.h>
#include <packet_protocol.h>
#include <malloc.h>
#include <android/log.h>

JNIEXPORT jbyteArray JNICALL
Java_edu_umn_fmcw_PacketProtocol_encodeEchoRequest(JNIEnv *env, jclass clazz,
                                                   jint uid) {
    RequestPacket rp = {
      .kind = PK_ECHO,
      .uid = uid,
      .revision = PACKET_PROTOCOL_REVISION,
      .contents_len = 5
    };
    memcpy(rp.contents.echo.message, "echo", 5);
    uint8_t buf[PACKET_REQUEST_MAX_LEN];
    int len = packet_encode_request(buf, &rp);

    if (len > 0) {
        jbyteArray result = (*env)->NewByteArray(env, 9);
        (*env)->SetByteArrayRegion(env, result, 0, len, (int8_t*)buf);
        return result;
    } else {
        return NULL;
    }
}

JNIEXPORT jbyteArray JNICALL
Java_edu_umn_fmcw_PacketProtocol_encodeWriteIntRequest(JNIEnv *env, jclass clazz,
                                                       jint uid, jint parameter, jint int_val) {
    RequestPacket rp = {
      .kind = PK_WRITE_PARAMETER,
      .uid = uid,
      .revision = PACKET_PROTOCOL_REVISION,
      .contents_len = 7
    };
    rp.contents.write_parameter.parameter = (uint16_t)parameter;
    rp.contents.write_parameter.param_kind = PKIND_INT32;
    rp.contents.write_parameter.int_val = int_val;
    uint8_t buf[PACKET_REQUEST_MAX_LEN];
    int len = packet_encode_request(buf, &rp);

    if (len > 0) {
        jbyteArray result = (*env)->NewByteArray(env, len);
        (*env)->SetByteArrayRegion(env, result, 0, len, (int8_t*)buf);
        return result;
    } else {
        return NULL;
    }
}

JNIEXPORT jbyteArray JNICALL
Java_edu_umn_fmcw_PacketProtocol_encodeWriteBoolRequest(JNIEnv *env, jclass clazz,
                                                        jint uid, jint parameter, jbyte bool_val) {
    RequestPacket rp = {
      .kind = PK_WRITE_PARAMETER,
      .uid = uid,
      .revision = PACKET_PROTOCOL_REVISION,
      .contents_len = 4
    };
    rp.contents.write_parameter.parameter = (uint16_t)parameter;
    rp.contents.write_parameter.param_kind = PKIND_BOOL;
    rp.contents.write_parameter.bool_val = bool_val ? (uint8_t)1 : (uint8_t)0;
    uint8_t buf[PACKET_REQUEST_MAX_LEN];
    int len = packet_encode_request(buf, &rp);

    if (len > 0) {
        jbyteArray result = (*env)->NewByteArray(env, len);
        (*env)->SetByteArrayRegion(env, result, 0, len, (int8_t*)buf);
        return result;
    } else {
        return NULL;
    }
}

JNIEXPORT jbyteArray JNICALL
Java_edu_umn_fmcw_PacketProtocol_encodeReadRequest(JNIEnv *env, jclass clazz,
                                                   jint uid, jint parameter, jbyte param_kind) {
    RequestPacket rp = {
      .kind = PK_READ_PARAMETER,
      .uid = uid,
      .revision = PACKET_PROTOCOL_REVISION,
      .contents_len = 3
    };
    rp.contents.read_parameter.parameter = (uint16_t)(parameter & 0xFFFF);
    rp.contents.read_parameter.param_kind = (uint8_t)param_kind;
    uint8_t buf[PACKET_REQUEST_MAX_LEN];
    int len = packet_encode_request(buf, &rp);

    if (len > 0) {
        jbyteArray result = (*env)->NewByteArray(env, len);
        (*env)->SetByteArrayRegion(env, result, 0, len, (int8_t*)buf);
        return result;
    } else {
        return NULL;
    }
}

JNIEXPORT jint JNICALL
Java_edu_umn_fmcw_PacketProtocol_getPacketProtocolRevision(JNIEnv *env, jclass clazz) {
  return PACKET_PROTOCOL_REVISION;
}

JNIEXPORT jint JNICALL
Java_edu_umn_fmcw_PacketProtocol_decodeResponse(JNIEnv *env, jclass clazz, jobject response,
                                                    jbyteArray arr, jint length) {
  jbyte *buf = (*env)->GetByteArrayElements(env, arr, NULL);

  ResponsePacket rp = {};
  int result = packet_decode_response((uint8_t*)buf, length, &rp);
  if (result > 0) {
    // write fields of responsepacket into the response jobject
    jclass cls = (*env)->GetObjectClass(env, response);
    jfieldID fid_kind = (*env)->GetFieldID(env, cls, "kind", "B");
    jfieldID fid_uid = (*env)->GetFieldID(env, cls, "uid", "B");
    jfieldID fid_revisionNumber = (*env)->GetFieldID(env, cls, "revisionNumber", "B");
    jfieldID fid_contentsLen = (*env)->GetFieldID(env, cls, "contentsLen", "B");

    (*env)->SetByteField(env, response, fid_kind, rp.kind);
    (*env)->SetByteField(env, response, fid_uid, rp.uid);
    (*env)->SetByteField(env, response, fid_revisionNumber, rp.revision);
    (*env)->SetByteField(env, response, fid_contentsLen, rp.contents_len);

    if (rp.kind == PK_ECHO) {
      jmethodID mid = (*env)->GetMethodID(env, cls, "jniSetEchoBody", "(Ljava/lang/String;)V");
      jstring jstr = (*env)->NewStringUTF(env, rp.contents.echo.message);
      (*env)->CallVoidMethod(env, response, mid, jstr);
      (*env)->DeleteLocalRef(env, jstr);
    } else if (rp.kind == PK_READ_PARAMETER) {
      jmethodID mid;
      jbyteArray jarr;
      switch (rp.contents.read_parameter.param_kind) {
        case PKIND_INT32:
          mid = (*env)->GetMethodID(env, cls, "jniSetReadIntBody", "(III)V");
          (*env)->CallVoidMethod(env, response, mid,
             rp.contents.read_parameter.parameter,
             rp.contents.read_parameter.param_kind,
             rp.contents.read_parameter.int_val);
          break;
        case PKIND_BOOL:
          mid = (*env)->GetMethodID(env, cls, "jniSetReadBoolBody", "(IIZ)V");
          (*env)->CallVoidMethod(env, response, mid,
             rp.contents.read_parameter.parameter,
             rp.contents.read_parameter.param_kind,
             rp.contents.read_parameter.bool_val);
          break;
        case PKIND_BYTES:
          mid = (*env)->GetMethodID(env, cls, "jniSetReadBytesBody", "(II[B)V");
          jarr = (*env)->NewByteArray(env, rp.contents_len - 3);
          (*env)->SetByteArrayRegion(env, jarr, 0, rp.contents_len - 3, (jbyte*)rp.contents.read_parameter.bytes_val);
          (*env)->CallVoidMethod(env, response, mid,
                                 rp.contents.read_parameter.parameter,
                                 rp.contents.read_parameter.param_kind,
                                 jarr);
          break;
        default:
          mid = (*env)->GetMethodID(env, cls, "jniSetReadBody", "(II)V");
          (*env)->CallVoidMethod(env, response, mid,
             rp.contents.read_parameter.parameter,
             rp.contents.read_parameter.param_kind);
      }
    } else if (rp.kind == PK_WRITE_PARAMETER) {
      jmethodID mid = (*env)->GetMethodID(env, cls, "jniSetWriteBody", "(II)V");
      (*env)->CallVoidMethod(env, response, mid,
        rp.contents.write_parameter.parameter,
        rp.contents.write_parameter.param_kind);
    } else if (rp.kind == PK_PRINT) {
      jmethodID mid = (*env)->GetMethodID(env, cls, "jniPrintPacket", "(Ljava/lang/String;)V");
      jstring jstr = (*env)->NewStringUTF(env, rp.contents.print.str);
      (*env)->CallVoidMethod(env, response, mid, jstr);
      (*env)->DeleteLocalRef(env, jstr);
    }
  }

  (*env)->ReleaseByteArrayElements(env, arr, buf, 0);

  return result;
}